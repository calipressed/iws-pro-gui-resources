import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';

@Component({
  selector: 'iw-typo',
  templateUrl: './typo.component.html',
  styleUrls: ['./typo.component.scss']
})
export class TypoComponent implements OnInit {
  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  ngOnInit() {
  }
}
