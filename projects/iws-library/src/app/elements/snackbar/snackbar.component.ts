import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'iw-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public snackBar: MatSnackBar,
      public ui: UilibService
  ) { }

  /**
   * Shows a message as snackbar.
   *
   * @param message – The message to be displayed.
   * @param action – The text of the dismiss action.
   * @param duration – Duration to display the snackbar (default: 1500ms)
   */
  showSnackbar(message, action, duration = 2500) {
    this.snackBar.open(message, action, {
      duration
    });
  }

  ngOnInit() {
  }

}
