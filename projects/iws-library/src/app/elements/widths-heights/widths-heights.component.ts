import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';

@Component({
  selector: 'iw-widths-heights',
  templateUrl: './widths-heights.component.html',
  styleUrls: ['./widths-heights.component.scss']
})
export class WidthsHeightsComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  ngOnInit() {
  }

}
