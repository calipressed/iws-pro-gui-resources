import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidthsHeightsComponent } from './widths-heights.component';

describe('WidthsHeightsComponent', () => {
  let component: WidthsHeightsComponent;
  let fixture: ComponentFixture<WidthsHeightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidthsHeightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidthsHeightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
