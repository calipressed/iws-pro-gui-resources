import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';

@Component({
  selector: 'iw-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  showSidebar = true;

  constructor(
      public ui: UilibService
  ) { }

  /**
   * Toggles the visibility of the sidebar.
   */
  toggleSidebar() {
    this.showSidebar = !this.showSidebar;
  }

  ngOnInit() {
  }

}
