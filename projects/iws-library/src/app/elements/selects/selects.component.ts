import { Component, OnInit } from '@angular/core';
import {UilibService} from '../../services/uilib.service';

@Component({
  selector: 'iw-selects',
  templateUrl: './selects.component.html',
  styleUrls: ['./selects.component.scss']
})
export class SelectsComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  exampleSelect = [
    {id: 1, name: 'Entry 01'},
    {id: 2, name: 'Entry 02'},
    {id: 3, name: 'Entry 03'},
    {id: 4, name: 'Entry 04'},
  ];

  exampleSelect2 = [
    {id: 1, name: 'Entry 01'},
    {id: 2, name: 'Entry 02'},
    {id: 3, name: 'Entry 03'},
    {id: 4, name: 'Entry 04'},
  ];



  ngOnInit() {
  }

}
