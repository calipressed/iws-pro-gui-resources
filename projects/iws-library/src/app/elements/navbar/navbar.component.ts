import { Component, OnInit } from '@angular/core';
import {UilibService} from '../../services/uilib.service';

@Component({
  selector: 'iw-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  ngOnInit() {
  }

}
