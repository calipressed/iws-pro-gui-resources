import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';

@Component({
  selector: 'iw-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  tabsVertical = [
    {name: 'Tab 01', icon: 'build', active: true},
    {name: 'Tab 02', icon: 'picture_as_pdf', active: false},
    {name: 'Tab 03', icon: 'shopping_basket', active: false},
    {name: 'Tab 04', icon: 'perm_media', active: false}
  ];

  tabsVertical2 = [
    {name: 'Tab 01', icon: 'build', active: true},
    {name: 'Tab 02', icon: 'picture_as_pdf', active: false},
    {name: 'Tab 03', icon: 'shopping_basket', active: false},
    {name: 'Tab 04', icon: 'perm_media', active: false}
  ];


  constructor(
      public ui: UilibService
  ) { }

  /**
   * Shows the tab content
   * @param index – The index of the selected tab.
   * @param array – The selected content array.
   */
  showTabContent(index, array) {
    if (array === 'tabsVertical') {
      this.tabsVertical.forEach(elem => {
        elem.active = false;
      });

      this.tabsVertical[index].active = true;
    } else {
      this.tabsVertical2.forEach(elem => {
        elem.active = false;
      });

      this.tabsVertical2[index].active = true;
    }
  }

  ngOnInit() {
  }

}
