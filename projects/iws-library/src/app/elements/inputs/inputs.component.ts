import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'iw-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent implements OnInit {
  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  user = new FormControl('', [Validators.required, Validators.email, Validators.pattern('max@mustermann.de')]);

  constructor(
      public ui: UilibService
  ) { }

  /**
   * Checks if a valid user name is entered
   *
   * @returns The error message as string.
   */
  getUserError() {
    return this.user.hasError('required') ? 'Bitte geben Sie Ihren Benutzernamen ein.' :
        this.user.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail-Adresse ein.' :
            this.user.hasError('pattern') ? 'Kein Benutzer mit dieser E-Mail-Adresse gefunden!' :
                '';
  }

  ngOnInit() {
  }
}
