import { Component, OnInit } from '@angular/core';
import {UilibService} from '../../services/uilib.service';

@Component({
  selector: 'iw-radios-checkboxes',
  templateUrl: './radios-checkboxes.component.html',
  styleUrls: ['./radios-checkboxes.component.scss']
})
export class RadiosCheckboxesComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  radioExample = ['Radio 01', 'Radio 02', 'Radio 03'];
  radioSelected = 'Radio 01';

  ngOnInit() {
  }

}
