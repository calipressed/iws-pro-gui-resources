import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiosCheckboxesComponent } from './radios-checkboxes.component';

describe('RadiosCheckboxesComponent', () => {
  let component: RadiosCheckboxesComponent;
  let fixture: ComponentFixture<RadiosCheckboxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadiosCheckboxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiosCheckboxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
