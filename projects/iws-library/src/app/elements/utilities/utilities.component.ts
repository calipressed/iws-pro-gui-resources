import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';

@Component({
  selector: 'iw-utilities',
  templateUrl: './utilities.component.html',
  styleUrls: ['./utilities.component.scss']
})
export class UtilitiesComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  ngOnInit() {
  }

}
