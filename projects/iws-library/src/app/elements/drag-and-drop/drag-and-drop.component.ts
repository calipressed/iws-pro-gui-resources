import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'iw-drag-and-drop',
  templateUrl: './drag-and-drop.component.html',
  styleUrls: ['./drag-and-drop.component.scss']
})
export class DragAndDropComponent implements OnInit {
  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  exampleItems = [
    {id: 1, active: true},
    {id: 2, active: false},
    {id: 3, active: false},
    {id: 4, active: false},
    {id: 5, active: false},
    {id: 6, active: false},
    {id: 7, active: false}
  ];

  constructor(
      public ui: UilibService
  ) { }

  /**
   * Sets the selected entry active.
   *
   * @param id – Id of the selected entry.
   */
  setSelectedEntry(id) {
    const index = this.exampleItems.findIndex(x => x.id === id);

    this.exampleItems.forEach(elem => {
      elem.active = false;
    });

    this.exampleItems[index].active = true;
  }

  /**
   * Updates the array when the item order is changed.
   *
   * @param event – The dropped element.
   */
  dropElem(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.exampleItems, event.previousIndex, event.currentIndex);
  }

  ngOnInit() {
  }
}
