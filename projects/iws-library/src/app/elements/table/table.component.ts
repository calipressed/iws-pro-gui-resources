import { Component, OnInit, ViewChild } from '@angular/core';
import { UilibService } from '../../services/uilib.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'iw-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})


export class TableComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  demoTable1 = [
    {id: 1, name: 'Aaaa', count: 60},
    {id: 2, name: 'Bbbb', count: 50},
    {id: 3, name: 'Cccc', count: 40},
    {id: 4, name: 'Dddd', count: 30},
    {id: 5, name: 'Eeee', count: 20},
    {id: 6, name: 'Ffff', count: 10},
  ];

  demoTable2 = [
    {id: 1, name: 'Aaaa', count: 60},
    {id: 2, name: 'Bbbb', count: 50},
    {id: 3, name: 'Cccc', count: 40},
    {id: 4, name: 'Dddd', count: 30},
    {id: 5, name: 'Eeee', count: 20},
    {id: 6, name: 'Ffff', count: 10},
  ];

  demoTable3 = [
    {id: 1, name: 'Aaaa', count: 60},
    {id: 2, name: 'Bbbb', count: 50},
    {id: 3, name: 'Cccc', count: 40},
    {id: 4, name: 'Dddd', count: 30},
    {id: 5, name: 'Eeee', count: 20},
    {id: 6, name: 'Ffff', count: 10},
  ];

  demoColumns1: string[] = ['id', 'name', 'count'];
  demoColumns2: string[] = ['id', 'name', 'count'];
  demoColumns3: string[] = ['id', 'name', 'count'];
  dataSource1 = new MatTableDataSource(this.demoTable1);
  dataSource2 = new MatTableDataSource(this.demoTable2);
  dataSource3 = new MatTableDataSource(this.demoTable3);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource3.sort = this.sort;
    this.dataSource3.paginator = this.paginator;
  }

}
