import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalDefaultComponent } from './modal-default/modal-default.component';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import {UilibService} from '../../services/uilib.service';

@Component({
  selector: 'iw-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService,
      public dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  /**
   * Opens the default modal
   *
   * @param title – The modal title.
   * @param msg – The modal message.
   */
  openModal(title, msg) {
    const dialogRef = this.dialog.open(ModalDefaultComponent, {
      width: '350px',
      data: {
        title,
        message: msg }
    });
  }

  /**
   * Opens the confirm modal
   *
   * @param title – The modal title.
   * @param msg – The modal message.
   */
  confirmModal(title, msg) {
    const dialogRef = this.dialog.open(ModalConfirmComponent, {
      width: '350px',
      data: {
        title,
        message: msg }
    });
  }
}
