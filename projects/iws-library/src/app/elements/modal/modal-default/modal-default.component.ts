import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'iw-modal-default',
  templateUrl: './modal-default.component.html',
  styleUrls: ['./modal-default.component.scss']
})
export class ModalDefaultComponent implements OnInit {

  constructor(
      public dialogRef: MatDialogRef<ModalDefaultComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
  }

}
