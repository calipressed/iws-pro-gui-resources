import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'iw-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss']
})
export class ModalConfirmComponent implements OnInit {

  constructor(
      public dialogRef: MatDialogRef<ModalConfirmComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
  }

}
