import { Component, OnInit } from '@angular/core';
import { UilibService } from '../../services/uilib.service';

@Component({
  selector: 'iw-scrollbar',
  templateUrl: './scrollbar.component.html',
  styleUrls: ['./scrollbar.component.scss']
})
export class ScrollbarComponent implements OnInit {
  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  constructor(
      public ui: UilibService
  ) { }

  ngOnInit() {
  }
}
