import { Injectable } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';


@Injectable({
  providedIn: 'root'
})
export class UilibService {

  // UI Elements
  elements: any[] = [
    {id: 0, name: 'Colors', active: true},
    {id: 1, name: 'Typo', active: false},
    {id: 2, name: 'Icons', active: false},
    {id: 3, name: 'Badges', active: false},
    {id: 4, name: 'Buttons', active: false},
    {id: 5, name: 'Cards', active: false},
    {id: 6, name: 'Datepicker', active: false},
    {id: 7, name: 'Drag & Drop', active: false},
    {id: 8, name: 'Dropdowns', active: false},
    {id: 9, name: 'Expansion Panel', active: false},
    {id: 10, name: 'Inputs', active: false},
    {id: 11, name: 'List Groups', active: false},
    {id: 12, name: 'Modal', active: false},
    {id: 13, name: 'Navbar', active: false},
    {id: 14, name: 'Pagination', active: false},
    {id: 15, name: 'Radios & Checkboxes', active: false},
    {id: 16, name: 'Scrollbar', active: false},
    {id: 17, name: 'Sidenav', active: false},
    {id: 18, name: 'Selects', active: false},
    {id: 19, name: 'Slide Toggle', active: false},
    {id: 20, name: 'Snackbar', active: false},
    {id: 21, name: 'Table', active: false},
    {id: 22, name: 'Tabs', active: false},
    {id: 23, name: 'Tools', active: false},
    {id: 24, name: 'Tooltips', active: false},
    {id: 25, name: 'Utilities', active: false},
    {id: 26, name: 'Widths & Heights', active: false},
  ];

  // Active UI Element
  activeElem = 0;

  // Manage Sidebar
  showSidebar = true;
  sidebarMode = 'side';
  hasBackdrop = false;
  showOverlay = false;
  isMobile = false;

  constructor(
      private clipboardService: ClipboardService
  ) { }

  /**
   * Sets the selected entry active
   * @param index – The index of the selected element.
   */
  setActive(index) {
    this.elements.forEach(elem => {
      elem.active = false;
    });

    this.elements[index].active = true;
    this.activeElem = index;

    if (this.isMobile ) {
      this.showSidebar = false;
    }
  }

  /**
   * Copies the content of the selected textarea
   * @param target – The id of the textarea.
   */
  copyHtml(target) {
    const text = document.querySelectorAll(target);
    this.clipboardService.copyFromContent(text[0].innerText);
  }

  /**
   * Toggles the visibility of the sidebar.
   */
  toggleSidebar() {
    this.showSidebar = !this.showSidebar;
  }
}
