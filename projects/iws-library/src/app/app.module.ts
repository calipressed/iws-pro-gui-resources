import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule} from './layout/layout.module';
import { LayoutComponent } from './layout/layout.component';
import { ButtonsComponent } from './elements/buttons/buttons.component';
import { ColorsComponent } from './elements/colors/colors.component';
import { TypoComponent } from './elements/typo/typo.component';
import { IconsComponent } from './elements/icons/icons.component';
import { BadgesComponent } from './elements/badges/badges.component';
import { CardsComponent } from './elements/cards/cards.component';
import { DatepickerComponent } from './elements/datepicker/datepicker.component';
import { DragAndDropComponent } from './elements/drag-and-drop/drag-and-drop.component';
import { DropdownsComponent } from './elements/dropdowns/dropdowns.component';
import { ExpansionPanelComponent } from './elements/expansion-panel/expansion-panel.component';
import { InputsComponent } from './elements/inputs/inputs.component';
import { ListGroupsComponent } from './elements/list-groups/list-groups.component';
import { ScrollbarComponent } from './elements/scrollbar/scrollbar.component';
import { ModalComponent } from './elements/modal/modal.component';
import { NavbarComponent } from './elements/navbar/navbar.component';
import { PaginationComponent } from './elements/pagination/pagination.component';
import { RadiosCheckboxesComponent } from './elements/radios-checkboxes/radios-checkboxes.component';
import { SidenavComponent } from './elements/sidenav/sidenav.component';
import { SelectsComponent } from './elements/selects/selects.component';
import { SlideToggleComponent } from './elements/slide-toggle/slide-toggle.component';
import { SnackbarComponent } from './elements/snackbar/snackbar.component';
import { TableComponent } from './elements/table/table.component';
import { TabsComponent } from './elements/tabs/tabs.component';
import { ToolsComponent } from './elements/tools/tools.component';
import { TooltipsComponent } from './elements/tooltips/tooltips.component';
import { UtilitiesComponent } from './elements/utilities/utilities.component';
import { ModalDefaultComponent } from './elements/modal/modal-default/modal-default.component';
import { ModalConfirmComponent } from './elements/modal/modal-confirm/modal-confirm.component';
import { WidthsHeightsComponent } from './elements/widths-heights/widths-heights.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    ButtonsComponent,
    ColorsComponent,
    TypoComponent,
    IconsComponent,
    BadgesComponent,
    CardsComponent,
    DatepickerComponent,
    DragAndDropComponent,
    DropdownsComponent,
    ExpansionPanelComponent,
    InputsComponent,
    ListGroupsComponent,
    ScrollbarComponent,
    ModalComponent,
    NavbarComponent,
    PaginationComponent,
    RadiosCheckboxesComponent,
    SidenavComponent,
    SelectsComponent,
    SlideToggleComponent,
    SnackbarComponent,
    TableComponent,
    TabsComponent,
    ToolsComponent,
    TooltipsComponent,
    UtilitiesComponent,
    ModalDefaultComponent,
    ModalConfirmComponent,
    WidthsHeightsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

@NgModule({})
export class LibraryShared {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: []
    };
  }
}