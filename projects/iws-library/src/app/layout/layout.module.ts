import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClipboardModule } from 'ngx-clipboard';

import {
  BsDropdownModule,
  CollapseModule,
  BsDatepickerModule,
  PaginationModule,
  SortableModule,
} from 'ngx-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatTooltipModule,
  MatSidenavModule,
  MatTabsModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatExpansionModule,
  MatSnackBarModule,
  MatDialogModule,
  MatListModule,
  MatCheckboxModule,
  MatRadioModule,
  MatSlideToggleModule
} from '@angular/material';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ModalDefaultComponent} from '../elements/modal/modal-default/modal-default.component';
import {ModalConfirmComponent} from '../elements/modal/modal-confirm/modal-confirm.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ClipboardModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CollapseModule.forRoot(),
    CommonModule,
    PaginationModule.forRoot(),
    SortableModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatSidenavModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatDialogModule,
    MatListModule,
    MatSlideToggleModule,
    NgSelectModule,
    NgxDropzoneModule,
    PerfectScrollbarModule
  ],
  entryComponents: [
    ModalDefaultComponent,
    ModalConfirmComponent
  ],
  exports: [
    ClipboardModule,
    BsDropdownModule,
    BsDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatTabsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatSelectModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatDialogModule,
    MatListModule,
    MatTooltipModule,
    MatSlideToggleModule,
    PaginationModule,
    NgSelectModule,
    NgxDropzoneModule,
    SortableModule,
    DragDropModule,
    PerfectScrollbarModule
  ]
})
export class LayoutModule { }
