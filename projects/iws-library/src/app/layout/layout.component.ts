import { Component, HostListener, OnInit } from '@angular/core';
import { UilibService } from '../services/uilib.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'iw-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  get data() {
    return this.ui;
  }

  set data(data: UilibService) {
    this.ui = data;
  }

  public screenWidth$ = new BehaviorSubject<number>
  (window.innerWidth);

  constructor(
      public ui: UilibService
  ) { }

  ngOnInit() {
    // Adjust sidebar visibility depending on screen width.
    this.getScreenWidth().subscribe(width => {
      if (width < 1200) {
        this.ui.showSidebar = false;
        this.ui.sidebarMode = 'push';
        this.ui.hasBackdrop = true;
        this.ui.isMobile = true;
      } else if (width > 1200) {
        this.ui.showSidebar = true;
        this.ui.sidebarMode = 'side';
        this.ui.hasBackdrop = false;
        this.ui.isMobile = false;
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }

  getScreenWidth(): Observable<number> {
    return this.screenWidth$.asObservable();
  }
}
