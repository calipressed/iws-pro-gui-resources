import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DetailsComponent } from './pages/details/details.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { SettingsComponent } from './pages/settings/settings.component';

const routes: Routes = [
    {
        path: 'prototype',
        component: LoginComponent
    },
    {
        path: 'prototype/login',
        component: LoginComponent
    },
    {
        path: 'prototype/listing',
        component: AppComponent
    },
    {
        path: 'prototype/dashboard',
        component: DashboardComponent
    },
    {
        path: 'prototype/details',
        component: DetailsComponent
    },
    {
        path: 'prototype/contacts',
        component: ContactsComponent
    },
    {
        path: 'prototype/settings',
        component: SettingsComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
