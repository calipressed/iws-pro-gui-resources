import { Injectable } from '@angular/core';
import { DemoDataService } from './demo-data.service';
import { SidebarService } from './sidebar.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  // Manage Sidebar Interactions for Small Screens
  isSmallScreen = false;

  // Selected Group
  selectedGroup = this.demoData.companyGroups[0];
  groupIsSelected = false;

  // Selected Company
  selectedCompany = this.demoData.companies[0];
  companyIsSelected = false;
  showCompanyList = false;
  showAllCompanies = false;
  companiesToShow = [];

  // Selected Contact
  selectedContact = this.demoData.contacts[0];
  contactIsSelected = false;
  tempAddress = { street: '', zip: '', city: '', country: 0};
  showAllContacts = false;
  contactsToShow = [];

  // Show / Hide Contacts on Small Screens
  hideMobileContacts = true;

  // Use Company Data as Contact Address
  useCompanyData = false;

  // Manage Edit Forms
  showContactForm = false;
  showCompanyForm = false;

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  set side(data: SidebarService) {
    this.sidebarService = data;
  }

  get side(): SidebarService {
    return this.sidebarService;
  }

  constructor(
      public demoData: DemoDataService,
      public sidebarService: SidebarService
  ) { }


  /**
   * Toggles between group and company list
   */
  toggleView(selection, index) {
    this.showCompanyList = !this.showCompanyList;
    this.showAllContacts = false;
    this.companyIsSelected = false;
    this.contactIsSelected = false;
    this.showAllCompanies = false;

    if (index !== undefined) {
      this.selectedGroup = this.demoData.companyGroups[index];
      this.showCompanyList = true;
    } else {
      this.showCompanyList = false;
    }

    this.companiesToShow = this.demoData.getCompaniesByGroup(this.selectedGroup);

  }

  /**
   * Shows or hides the contact panel on small screens
   *
   * @param value – The value to be set.
   */
  switchMobileView(value) {
    this.hideMobileContacts = value;
  }

  /**
   * Shows the company form
   */
  showCompany() {
    this.showCompanyForm = true;
    this.showContactForm = false;
    this.showAllContacts = false;

    if (this.isSmallScreen) {
      this.sidebarService.showSidebar = false;
    }
  }

  /**
   * Hides the company form
   */
  hideCompany() {
    this.showCompanyForm = false;
    this.companyIsSelected = false;

    if (this.isSmallScreen) {
      this.sidebarService.showSidebar = true;
      this.hideMobileContacts = true;
    }
  }

  /**
   * Adds a new company
   */
  addCompany() {
    this.companyIsSelected = false;
    this.showCompany();
  }

  /**
   * Shows the data of the selected company
   *
   * @param id – The id of the selected company.
   */
  editCompany(id) {
    this.companyIsSelected = true;
    this.contactIsSelected = false;
    this.showCompany();
    this.selectedCompany = this.demoData.getCompanyById(id);
    this.contactsToShow = this.demoData.getContactsByCompany(this.selectedCompany.id);
  }

  /**
   * Shows the contacts form
   */
  showContact() {
    this.showContactForm = true;
    this.showCompanyForm = false;

    if (this.isSmallScreen) {
      this.sidebarService.showSidebar = false;
    }
  }

  /**
   * Hides the contacts form
   */
  hideContact() {
    this.showContactForm = false;
    this.contactIsSelected = false;

    if (this.isSmallScreen) {
      this.sidebarService.showSidebar = true;
    }
  }

  /**
   * Adds a new contact
   */
  addContact() {
    this.contactIsSelected = false;
    this.showContact();
    this.useCompanyData = false;
    this.getContactAddress();
  }

  /**
   * Shows the data of the selected contact
   *
   * @param id – The id of the selected contact.
   */
  editContact(id) {
    this.contactIsSelected = true;
    this.showContact();
    this.selectedContact = this.demoData.getContactById(id);

    if (this.selectedContact.useCompanyAddress) {
      this.useCompanyData = true;
      this.selectedCompany = this.demoData.getCompanyById(this.selectedContact.company);
    } else {
      this.useCompanyData = false;
    }

    this.getContactAddress();
  }

  /**
   * Manages the address data of the active contact
   */
  getContactAddress() {
    // Contact Uses Company Address
    if (this.useCompanyData) {
      this.tempAddress.street = this.selectedCompany.street;
      this.tempAddress.zip = this.selectedCompany.zip;
      this.tempAddress.city = this.selectedCompany.city;
      this.tempAddress.country = this.selectedCompany.country;

      // Contact Uses Own Address
    } else if (this.contactIsSelected) {
      this.tempAddress.street = this.selectedContact.street;
      this.tempAddress.zip = this.selectedContact.zip;
      this.tempAddress.city = this.selectedContact.city;
      this.tempAddress.country = this.selectedContact.country;

    // Contact Has No Address
    } else {
      this.tempAddress.street = '';
      this.tempAddress.zip = '';
      this.tempAddress.city = '';
      this.tempAddress.country = 0;
    }
  }

  /**
   * Shows all contacts of the selected group
   */
  showAllGroupContacts() {
    const companies = this.demoData.getCompaniesByGroup(this.selectedGroup);
    const contacts = [];

    companies.forEach(comp => {
      contacts.push(this.demoData.getContactsByCompany(comp.id));
    });

    this.companyIsSelected = true;
    this.showAllContacts = true;
    this.hideMobileContacts = false;

    this.contactsToShow = [].concat.apply([], contacts);
  }

  /**
   * Shows all companies.
   */
  showCompaniesAll() {
    this.companyIsSelected = true;
    this.showCompanyList = true;
    this.showAllContacts = false;
    this.companyIsSelected = false;
    this.contactIsSelected = false;

    this.showAllCompanies = true;
    this.companiesToShow = this.demoData.companies;
  }
}
