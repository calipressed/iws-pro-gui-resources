import { Injectable } from '@angular/core';
import { DemoDataService } from './demo-data.service';
import {MatTableDataSource} from "@angular/material";

@Injectable({
  providedIn: 'root'
})

export class ListingService {

    // Status List
    statusRange =
        [
            'All',
            'New',
            'Waiting for Data',
            'Preflight',
            'Waiting for Approval',
            'Approved',
            'Imposing',
            'Ready for Print',
            'Printing',
            'Finished',
            'Error'];

    // Highlighted Rows
    highlightedRows = [];

    // Listing: Table Header Columns
    displayedFileColumns: string[] = ['preview', 'id', 'fileName', 'productId', 'orderId', 'contact', 'fileStatus', 'fileType', 'link'];
    displayedOrderColumns: string[] = ['id', 'orderName', 'deadline', 'contact', 'productCount', 'fileCount', 'orderStatus', 'link'];
    displayedProductColumns: string[] = ['id', 'productName', 'order', 'contact', 'fileCount', 'productStatus', 'link'];

    // Dashboard: Table Header Columns
    dashboardOrderColumns: string[] = ['id', 'orderName', 'deadline', 'contact', 'link'];
    dashboardFileColumns: string[] = ['preview', 'id', 'fileName', 'orderId', 'link'];

    // Table Columns
    fileTableColumns: any [] = [
        {name: 'Datei ID', active: true},
        {name: 'Datei', active: true},
        {name: 'Produkt', active: true},
        {name: 'Bestellung', active: true},
        {name: 'Kontakt', active: true},
        {name: 'Status', active: true},
        {name: 'Deadline (Bestellung)', active: false},
        {name: 'Global Custom Value 01', active: false}
    ];

    productTableColumns: any [] = [
        {name: 'Produkt ID', active: true},
        {name: 'Produkt', active: true},
        {name: 'Bestellung', active: true},
        {name: 'Kontakt', active: true},
        {name: 'Dateien', active: true},
        {name: 'Status', active: true},
        {name: 'Deadline (Bestellung)', active: false},
        {name: 'Global Custom Value 01', active: false}
    ];

    orderTableColumns: any [] = [
        {name: 'Bestell ID', active: true},
        {name: 'Bestellung', active: true},
        {name: 'Deadline', active: true},
        {name: 'Kontakt', active: true},
        {name: 'Produkte', active: true},
        {name: 'Dateien', active: true},
        {name: 'Status', active: true},
        {name: 'Global Custom Value 01', active: false},
        {name: 'Global Custom Value 02', active: false}
    ];

    // Active Page
    activePage = 'listing';

    // Views
    activeViews: any[] = ['files', 'products', 'orders'];

    // Active View
    activeView = 'files';

    // Active Scope ('preview', 'edit', 'new')
    activeScope = 'preview';

    // Visibility of Preflight Report
    showPreflight = false;

    // Visibility for File Types
    showPdfFiles = true;
    showImposedFiles = true;

    // Selected Files by File ID
    selectedFiles: any[] = [];
    currentFile: any[] = [];
    currentFiles: any[] = [];
    activeFiles: any[] = [];

    // Selected Products by Product ID
    selectedProducts: any[] = [];
    currentProduct: any[] = [];
    currentProducts: any[] = [];
    activeProducts: any [] = [];

    // Selected Orders by Order ID
    selectedOrders: any[] = [];
    selectedOrder: any[] = [];
    currentOrder: any[] = [];
    activeOrder: any[] = [];

    // Mat Table Data Source for Files
    fileSource = new MatTableDataSource(this.demoData.getDemoFilesByType(0));

    // Deadline Placeholder
    orderDeadline = new Date();

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get data(): DemoDataService {
        return this.demoData;
    }

    constructor(
        public demoData: DemoDataService
    ) { }

    /**
     * Sets the scope for the sidebar overlay.
     *
     * @param scope – 'preview', 'edit' or 'new'.
     * @param fallback – The scope to be set if selected scope is already active.
     */
    setActiveScope(scope, fallback = 'undefined') {
        if (typeof fallback !== 'undefined') {
            if (this.activeScope === scope) {
                this.activeScope = fallback;
            } else {
                this.activeScope = scope;
            }
        } else {
            this.activeScope = scope;
        }
    }

    /**
     * Sets the active view.
     *
     * @param activeView – The active view ('files', 'products' or 'orders');
     */
    setActiveView(activeView) {
        this.activeView = activeView;
    }

    /**
     * Changes the active view when the active tab is changed.
     *
     * @param event – The change event.
     */
    setTabView(event) {
        this.activeView = this.activeViews[event.index];
    }

    /**
     * Manages the active state of the table columns.
     *
     * @remarks Utilized for the active states of the edit table dropdowns.
     *
     * @param columns – The column array.
     * @param name – The name of the value to be changed.
     * @param value – The value to be set (true / false).
     */
    setTableColumns(columns, name, value) {
        const index = columns.findIndex(x => x.name === name);
        columns[index].active = value;
    }

    /**
     * Adds / removes the selected item from the list of selected items.
     *
     * @remarks
     * Affects the arrays selectedFiles, selectedProducts or selectedOrders.
     *
     * @param item – The selected item.
     */
    manageSelectedItems(item) {
        let container;

        // Get the corresponding container
        switch (this.activeView) {
            case 'products': container = this.selectedProducts; break;
            case 'orders': container = this.selectedOrders; break;
            default: container = this.selectedFiles;
        }

        // Add / remove item
        if (container.includes(item.id)) {
            const pos = container.indexOf(item.id);
            container.splice(pos, 1);
        } else {
            container.push(item.id);
        }
    }

    /**
     * Sets table row active.
     *
     * @param row – The selected row.
     */
    setActiveRow(row) {
        if (this.highlightedRows.includes(row)) {
            this.highlightedRows.splice(this.highlightedRows.indexOf(row), 1);

        } else {
            this.highlightedRows.push(row);
        }
    }

    /**
     * Deselects all rows.
     * @param event – The mouse event.
     */
    deselectAllRows(event) {
        this.highlightedRows = [];
    }

    /**
     * Refreshes the file table.
     *
     * @param fileType = The selected file type.
     */
    updateFileTable(fileType) {
        this.fileSource.data = this.demoData.getDemoFilesByType(fileType);
    }

    /**
     * Sets the file type and updates the file table.
     *
     */
    setFileType() {
        if (this.showPdfFiles && !this.showImposedFiles) {
            this.updateFileTable(1);
        } else if (this.showImposedFiles && !this.showPdfFiles) {
            this.updateFileTable(2);
        } else if (!this.showPdfFiles && !this.showImposedFiles) {
            this.updateFileTable(-1);
        } else {
            this.updateFileTable(0);
        }
    }
}
