import { Injectable } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Show Main Navigation
  showMainNav = true;

  // Show Logout Button
  showLogout = true;

  // Show Login Mask
  showLogin = true;

  // Show Forgot PWD Mask
  forgotPwd = false;

  // Show Send PWD Info
  sendPwd = false;

  // Show Login after PWD has been send
  newPwd = false;

  // Temp User Name
  userName = 'admin@impressed.de';

  // Temp Password
  passWord = 'impressed';

  // Form Control: Username
  user = new FormControl('', [Validators.required, Validators.email, Validators.pattern(this.userName)]);

  // Form Control: Password
  pwd = new FormControl('', [Validators.required, Validators.pattern(this.passWord)]);

  // From Control: E-Mail
  email = new FormControl('', [Validators.required, Validators.email]);

  // Main Navigation Links
  mainNav: any[] = [
    {name: 'Übersicht', active: false, url: 'prototype/dashboard'},
    {name: 'Jobs', active: true, url: 'prototype/listing'},
    {name: 'Kontakte', active: false, url: 'prototype/contacts'},
    {name: 'Einstellungen', active: false, url: 'prototype/settings'}
  ];


  constructor() { }

  /**
   * Sets the active status of a page.
   *
   * @param page – The page to be changed.
   * @param value – The value to be set (true/false).
   */
  setActivePage(page, value): void {
    const index = this.mainNav.findIndex(x => x.name === page);
    this.mainNav[index].active = value;
  }


  /**
   * Checks if a valid user name is entered
   *
   * @returns The error message as string.
   */
  getUserError() {
       return this.user.hasError('required') ? 'Bitte geben Sie Ihren Benutzernamen ein.' :
        this.user.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail-Adresse ein.' :
          this.user.hasError('pattern') ? 'Kein Benutzer mit dieser E-Mail-Adresse gefunden!' :
            '';
  }

  /**
   * Checks if the password is valid
   *
   * @returns The error message as string.
   */
  getPwdError() {
    return this.pwd.hasError('required') ? 'Bitte geben Sie Ihr Passwort an.' :
        this.pwd.hasError('pattern') ? 'Falsches Passwort!' :
            '';
  }

  /**
   * Checks if the email address is valid
   *
   * @returns The error message as string.
   */
  getEmailError() {
    return this.email.hasError('required') ? 'Bitte geben Sie Ihren Benutzernamen ein.' :
        this.email.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail-Adresse ein.' :
              '';
  }

  /**
   * Enables / disables the login button
   *
   * @returns The disabled status (true / false).
   */
  enableLogin() {
    return (this.user.value === this.userName && this.pwd.value === this.passWord) ? false : true;
  }

}
