import { Injectable } from '@angular/core';
import { DemoDataService } from './demo-data.service';
import { ListingService } from './listing.service';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
    // Sidebar Config
    showSidebar = true;
    sidebarMode = 'side';
    hasBackdrop = false;
    showOverlay = false;
    hideSidebarWidth = 1200;

    // Page has sidebar
    hasSidebar = true;

    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get data(): DemoDataService {
        return this.demoData;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService
    ) { }


    /**
     * Toggles the visibility of the sidebar.
     */
    toggleSidebar() {
        this.showSidebar = !this.showSidebar;
    }

    /**
     * Opens or closes the sidebar overlay depending on the number of selected rows and the scope.
     * Retrieves the selected files, orders and products of the selected row.
     *
     * @param selectedRows – The selected row.
     */
     openOverlay(selectedRows) {

        if (selectedRows === 1) {
            this.showOverlay = true;
        } else if (selectedRows > 1 && this.listingService.activeScope === 'edit') {
            this.showOverlay = true;
        } else if (this.listingService.activeScope === 'new') {
            this.showOverlay = true;
        } else {
          this.closeOverlay();
        }

        // Fallback
        if (this.listingService.activeScope === 'undefined') {
            this.listingService.activeScope = 'preview';
        }

        // Get Element Data: Single File
        if (selectedRows === 1 && this.listingService.activeView === 'files') {
          this.listingService.currentFile =  this.demoData.getFile(this.listingService.selectedFiles[0]);
          this.listingService.currentProduct = this.demoData.getProductById(this.listingService.currentFile['productId']);
          this.listingService.currentOrder = this.demoData.getOrderById(this.listingService.currentProduct['orderId']);
          this.listingService.currentProducts = this.demoData.getProductsByOrderId(this.listingService.currentProduct['orderId']);
          this.listingService.activeFiles = this.demoData.getFilesByProductId(this.listingService.currentProduct['id']);

        // Get Element Data: Single Product
        } else if (selectedRows === 1 && this.listingService.activeView === 'products') {
          this.listingService.currentProduct = this.demoData.getProductById(this.listingService.selectedProducts[0]);
          this.listingService.selectedOrder = this.demoData.getOrderById(this.listingService.currentProduct['orderId']);
          this.listingService.currentFiles = this.demoData.getFilesByProductId(this.listingService.currentProduct['id']);

        // Get Element Data: Single Order
        } else if (selectedRows === 1 && this.listingService.activeView === 'orders') {
            this.listingService.activeOrder = this.demoData.getOrderById(this.listingService.selectedOrders[0]);
            this.listingService.activeProducts = this.demoData.getProductsByOrderId(this.listingService.activeOrder['id']);
            this.listingService.orderDeadline = new Date(this.listingService.activeOrder['deadline']);
        }
     }

    /**
     * Closes the sidebar overlay, resets the scope and preflight visibility.
     */
    closeOverlay() {
        this.showOverlay = false;
        this.listingService.showPreflight = false;
        this.listingService.setActiveScope('preview');
     }
}
