import { Injectable } from '@angular/core';
import { SidebarService } from './sidebar.service';
import { DemoDataService } from './demo-data.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  // Setting Navigation
  settingsNav: any[] = [
    {id: 1, name: 'Mein Profil', icon: 'perm_identity', sideNav: false, active: true, data: false},
    {id: 2, name: 'Preflight Einstellungen', icon: 'assignment', sideNav: true, active: false, data: this.demoData.preflightPresets},
    {id: 3, name: 'Workflows & Abläufe', icon: 'print', sideNav: true, active: false, data: this.demoData.workflows},
    {id: 4, name: 'Imposition Presets', icon: 'view_comfy', sideNav: true, active: false, data: this.demoData.imposePresets},
    {id: 5, name: 'Invididuelle Werte', icon: 'code', sideNav: true, active: false, data: this.demoData.customValues},
    {id: 6, name: 'Einstellungen', icon: 'edit', sideNav: false, active: false, data: false},
    {id: 7, name: 'Module', icon: 'build', sideNav: true, active: false, data: this.demoData.modules},
    {id: 8, name: 'Benutzer', icon: 'group', sideNav: true, active: false, data: this.demoData.user},
    {id: 9, name: 'Entwickler', icon: 'build', sideNav: false, active: false, data: false},
    {id: 10, name: 'Werkzeuge', icon: 'settings', sideNav: true, active: false, data: this.demoData.settingTools},
  ];

  // Active Navigation
  activeNav = this.settingsNav[0];

  // Selected Subnav Entry
  selectedSubNav = 0;

  // Manage Sidebar Interactions for Small Screens
  isSmallScreen = false;

  // Show / Hide Subnavigation on Small Screens
  hideMobileNav = true;

  // Add new Entry
  newEntry = false;

  // Show Mask for New / Edit Entries
  showEditMask = true;

  // Hide Search
  hideSearch = true;

  // Demo Values for Preflight Prests
  preflightTac = false;;
  preflightResolution = false;
  preflightOffset = false;

  // Demo Values for Impose Presets
  imposeOption01 = true;
  imposeOption02 = false;

  // Values for Drag & Drop List (for Workflows & Custom Values)
  entrySelected = false;
  newEntryItem = false;
  fixedValues = false;
  selectedStatus = this.demoData.statusRange[0];
  selectedCustom = this.demoData.customValues[0];
  selectedValue = [];

  set side(data: SidebarService) {
    this.sidebarService = data;
  }

  get side(): SidebarService {
    return this.sidebarService;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  constructor(
      public sidebarService: SidebarService,
      public demoData: DemoDataService
  ) { }

  /**
   * Sets the selected navigation active
   *
   * @param index – The selected entry.
   */
  setActiveNav(index) {
    this.settingsNav.forEach(nav => {
      nav.active = false;
    });

    this.selectedSubNav = 0;

    this.settingsNav[index].active = true;
    this.activeNav = this.settingsNav[index];

    if (this.isSmallScreen) {
      if (this.activeNav.sideNav) {
        this.hideMobileNav = false;
      } else {
        this.sidebarService.showSidebar = false;
      }
    }

    if (!this.activeNav.sideNav) {
      this.showEditMask = true;
    } else {
      this.showEditMask = false;
    }
  }

  /**
   * Sets the selected subnav active
   *
   * @param index – The selected entry.
   */
  selectSubnav(index) {
    this.selectedSubNav = index;
    this.newEntry = false;
    this.showEditMask = true;
    this.setValues(this.activeNav.id);
    this.entrySelected = false;
    this.newEntryItem = false;
    this.selectedCustom = this.activeNav.data[this.selectedSubNav];
    this.fixedValues = false;

    if (this.isSmallScreen) {
      this.sidebarService.showSidebar = false;
    }
  }

  /**
   * Shows or hides the subnavigation on small screens
   *
   * @param value – The value to be set.
   */
  switchMobileView(value) {
    this.hideMobileNav = value;
  }

  /**
   * Shows the input mask for a new entry
   */
  addNewEntry() {
    this.newEntry = true;
    this.showEditMask = true;
    this.selectedSubNav = 0;
    this.entrySelected = false;
    this.newEntryItem = false;
    this.fixedValues = false;

    this.setValues(this.activeNav.id);
    this.selectedCustom = this.demoData.customValues[0];

    if (this.isSmallScreen) {
      this.sidebarService.showSidebar = false;
    }
  }

  /**
   * Hides the details form.
   */
  hideDetails() {
    this.newEntry = false;
    this.selectedSubNav = 0;

    if (this.activeNav.sideNav) {
      this.showEditMask = false;
    }

    if (this.isSmallScreen && this.activeNav.sideNav) {
      this.sidebarService.showSidebar = true;
    }
  }

  /**
   * Sets the demo data for new & selected entries
   *
   * @param view – The index of the active view.
   */
  setValues(view) {
    // Options for Preflight Presets
    if ( view === 2) {
      if(this.newEntry) {
        this.preflightTac = false;
      }  else {
        this.preflightTac = true;
        this.preflightResolution = this.activeNav.data[this.selectedSubNav].resolution;
        this.preflightOffset = this.activeNav.data[this.selectedSubNav].offset;
      }
    }

    // Options for Impose Presets
    if ( view === 4 && this.newEntry) {
      this.imposeOption01 = false;
      this.preflightResolution = false;
      this.preflightOffset = false;
    }
  }

  /**
   * Updates the status array when the order is changed.
   *
   * @param event – The dropped element.
   */
  dropStatus(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.demoData.statusRange, event.previousIndex, event.currentIndex);
  }

  /**
   * Sets the value of the selected entry
   *
   * @param type – The selected type ('status' or 'custom').
   * @param value – The value to be set.
   */
  setSelectedEntry(type, value) {
    if (type === 'status') {
      this.entrySelected = true;
      this.selectedStatus = value;
    }

    if (type === 'value') {
      this.entrySelected = true;
      this.selectedValue = value;
    }
  }

  /**
   * Shows the input mask for a new list item
   */
  addNewItem() {
    this.entrySelected = false;
    this.newEntryItem = true;
  }
}
