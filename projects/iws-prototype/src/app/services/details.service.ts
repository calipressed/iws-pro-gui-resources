import { Injectable, ElementRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';


@Injectable({
  providedIn: 'root'
})
export class DetailsService {

  // Sidebar: Show Edit Views for Order & Product
  editOrder = false;
  editProduct = false;

    // Thumbnail Demo Data
  thumbnails: any[] = [
    {page: 1, path: '175877-1_0001.jpeg', connect: false, selected: false, show: true},
    {page: 2, path: '175877-1_0002.jpeg', connect: false, selected: false, show: false},
    {page: 3, path: '175877-1_0004.jpeg', connect: false, selected: false, show: false},
    {page: 4, path: '175877-1_0001.jpeg', connect: false, selected: false, show: false},
    {page: 5, path: '175877-1_0002.jpeg', connect: false, selected: false, show: false},
    {page: 6, path: '175877-1_0004.jpeg', connect: false, selected: false, show: false},
    {page: 7, path: '175877-1_0001.jpeg', connect: false, selected: false, show: false},
    {page: 8, path: '175877-1_0002.jpeg', connect: false, selected: false, show: false},
    {page: 9, path: '175877-1_0004.jpeg', connect: false, selected: false, show: false}


  ];

  // Thumbnail Width
  thumbWidth = 135;
  thumbImageWidth = 115;

  // Thumbnails per Row
  thumbsPerRow: number;

  // Thumbnail Container
  thumbsContainer: ElementRef;

  // Position for Thumbnail Tools
  toolWidth = 50;
  toolHeight = 45;
  rowHeight = 158;

  toolPosition = {
    top: '35px',
    left: '0px'
  };

  // Visibility of the Thumbnail Tools
  showDeleteTool = false;
  showLockTool = false;
  showUnlockTool = false;

  constructor() { }


  /**
   * Calculates how many thumbnails are in a row.
   */
  calcThumbsPerRow() {
    const thumbnailCount = this.thumbnails.length;
    const containerWidth = this.thumbsContainer.nativeElement.offsetWidth;
    const newCount = Math.floor(containerWidth / this.thumbWidth);

    if (Math.round(thumbnailCount / 2) > newCount ) {
      this.thumbsPerRow = Math.round(thumbnailCount / 2);
    } else {
      this.thumbsPerRow = newCount;
    }
  }

  /**
   * Calculates the position for the tool overlay based on the last selected thumbnail.
   *
   * @param index – The index of the selected thumbnail
   */
  calcToolPosition(index) {
    // Number of Active Tools
    let numToolsActive = 1;
    if (this.showLockTool) { numToolsActive++; }
    if (this.showUnlockTool) { numToolsActive++; }

    // Row Position
    let leftPos = 0;
    let topPos = 1;

    if (index >= this.thumbsPerRow) {
      leftPos = index - this.thumbsPerRow;
      topPos = 2;
    } else {
      leftPos = index;
      topPos = 1;
    }

    // Calc Left Position
    const offsetLeft = this.thumbImageWidth / 2 - (this.toolWidth * numToolsActive / 2);
    this.toolPosition.left = this.thumbWidth * leftPos + offsetLeft + 'px';

    // Calc Top Position
    if (topPos > 1) {
      this.toolPosition.top = (this.thumbImageWidth / 2 - (this.toolHeight / 2)) + this.rowHeight + 'px';
    } else {
      this.toolPosition.top = (this.thumbImageWidth / 2 - (this.toolHeight / 2))  + 'px';
    }
  }

  /**
   * Checks if the next thumbnail is connected.
   *
   * @param index – Index of the current thumbnail.
   */
  thumbNextIsConnected(index) {
    if (this.thumbnails.length > index + 1) {
      return this.thumbnails[index + 1].connect;
    } else {
      return false;
    }
  }

  /**
   * Checks if the previous thumbnail is connected.
   *
   * @param index – Index of the current thumbnail.
   */
  thumbPrevIsConnected(index) {
    if (index > 0) {
      return this.thumbnails[index - 1].connect;
    } else {
      return false;
    }
  }

  /**
   * Shows / hides the selected thumbnail.
   *
   * @param index – Index of the selected thumbnail.
   */
  thumbShow(index) {
    this.thumbnails.forEach((thumb) => {
      thumb.show = false;
    });

    this.thumbnails[index].show = true;
  }

  /**
   * Gets the active thumbnail.
   *
   * @returns The image path.
   */
  getActiveThumb() {
    let noSelection = false;

    if (this.thumbnails.length > 0) {
      for (const thumb of this.thumbnails) {

        if (thumb.show === true) {
          noSelection = false;
          return thumb.path;
        } else {
          noSelection = true;
        }
      }
    } else {
      return 'no-pic.svg';
    }

    if (noSelection) {
      return 'no-pic.svg';
    }
  }

  /**
   * Deletes the selected thumbnail(s).
   */
  deleteThumb() {
    let count = 0;
    const newData = [];

    for (const {thumb, index} of this.thumbnails.map((thumb, index) => ({thumb, index}))) {
      if (!thumb.selected) {
        newData.push(thumb);
        count++;
      }
    }

    this.thumbnails = newData;

    if (count === this.thumbnails.length + 1) {
      this.thumbnails = [];
    }

    this.showDeleteTool = false;
    this.showLockTool = false;
    this.showUnlockTool = false;
  }

  /**
   * Checks if the selected thumbnails are connected.
   */
  checkConnection() {
    let count = 0;

    this.thumbnails.forEach((thumb, index) => {
      if (thumb.selected) {
        count++;

        // Check next thumbnail
        if (index + 1 < this.thumbnails.length) {
          if (this.thumbnails[index + 1].selected && this.thumbnails[index + 1].connect) {
            this.showUnlockTool = true;
          }

          if (this.thumbnails[index + 1].selected && !this.thumbnails[index + 1].connect) {
            this.showLockTool = true;
          }
        }

        // Check previous thumbnail
        if (index > 0) {
          if (this.thumbnails[index - 1].selected && this.thumbnails[index - 1].connect) {
            this.showUnlockTool = true;
          }

          if (this.thumbnails[index - 1].selected && !this.thumbnails[index - 1].connect) {
            this.showLockTool = true;
          }
        }

      }
    });

    if (count > 0) {
      this.showDeleteTool = true;

      if (count === 1) {
        this.showUnlockTool = false;
        this.showLockTool = false;
      }
    } else {
      this.showDeleteTool = false;
      this.showUnlockTool = false;
      this.showLockTool = false;

    }
  }

  /**
   * Connects the selected thumbnails.
   */
  connectThumbs() {
    for (const {thumb, index} of this.thumbnails.map((thumb, index) => ({thumb, index}))) {
      if (thumb.selected) {
        // Check if previous thumbnail is selected
        if (index > 0 && this.thumbnails[index - 1].selected) {
          thumb.connect = true;
        } else if (index < this.thumbnails.length && this.thumbnails[index + 1].selected) {
          thumb.connect = true;
        }
      }
    }

    this.showLockTool = false;
    this.showUnlockTool = true;
  }

  /**
   * Separates the selected thumbnails.
   */
  separateThumbs() {
    for (const {thumb, index} of this.thumbnails.map((thumb, index) => ({thumb, index}))) {
      if (thumb.selected) {
        // Check if previous thumbnail is selected
        if (index > 0 && this.thumbnails[index - 1].selected) {
          thumb.connect = false;
        } else if (index < this.thumbnails.length && this.thumbnails[index + 1].selected) {
          thumb.connect = false;
        }
      }
    }

    this.showLockTool = true;
    this.showUnlockTool = false;
  }

  /**
   * Updates the thumbnail array when the order is changed.
   *
   * @param event – The dropped element.
   */
  drop(event: CdkDragDrop<string[]>) {
    let fromPosition = event.previousIndex;
    let toPosition = event.currentIndex;

    // Move Item to First Row
    if (event.container.id === 'cdk-drop-list-0' && event.previousContainer.id === 'cdk-drop-list-1') {
      fromPosition = event.previousIndex + this.thumbsPerRow;
      toPosition = event.currentIndex;

    // Move Item to Second Row
    } else if (event.container.id === 'cdk-drop-list-1' && event.previousContainer.id === 'cdk-drop-list-0') {
      fromPosition = event.previousIndex;
      toPosition = event.currentIndex + this.thumbsPerRow;

    // Move Item In Second Row
    } else if (event.container.id === 'cdk-drop-list-1' && event.container.id === 'cdk-drop-list-1') {
      fromPosition = event.previousIndex + this.thumbsPerRow;
      toPosition = event.currentIndex + this.thumbsPerRow;
    }

    moveItemInArray(this.thumbnails, fromPosition, toPosition);
  }
}
