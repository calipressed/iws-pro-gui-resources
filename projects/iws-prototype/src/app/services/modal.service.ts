import { Injectable } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import {ConfirmDeleteComponent} from '../layout/modal/confirm-delete/confirm-delete.component';

@Injectable({
  providedIn: 'root'
})

export class ModalService {

    // Default delete message
    public deleteMessage = 'Möchten Sie die ausgewählten Elemente wirklich löschen?';

    constructor(
        public snackBar: MatSnackBar,
        public dialog: MatDialog
    ) {}

  /**
   * preventAutoClose()
   *
   * @remarks
   * This method sets the option "autoClose" of the BsDropdownDirective
   * so the BsDropdown is prevented from closing through the click on an
   * UI element within the dropdown (e.g. datepicker)
   *
   * @param elem – Child of the BsDropdownDirective.
   * @value – Value to be set (true or false).
   */
  preventAutoClose(elem, value) {
      if (value === false) {
          elem.autoClose = false;
      } else {
          setTimeout(() => {
              elem.autoClose = true;
          }, 10);
      }
  }

  /**
   * Shows a message as snackbar.
   *
   * @param message – The message to be displayed.
   * @param action – The text of the dismiss action.
   * @param duration – Duration to display the snackbar (default: 1500ms)
   */
  showSnackbar(message, action, duration = 2500) {
    this.snackBar.open(message, action, {
        duration
    });
  }

  /**
   * Opens the confirm delete dialog.
   *
   * @param message – The message to be displayed.
   * @param dialogWidth – The width of the dialog (default: 350px);
   */
  confirmDelete(message, dialogWidth = '350px') {

    if (message) {
        this.deleteMessage = message;
    }

    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
          width: dialogWidth,
          data: { message: this.deleteMessage }
    });

    dialogRef.afterClosed().subscribe(result => {});
  }
}
