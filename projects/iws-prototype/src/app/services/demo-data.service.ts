import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class DemoDataService {

    // Status
    statusRange: any[] = [
        {
            statusId: 'status-01',
            statusName: 'New',
            type: 'auto',
            locked: false
        },
        {
            statusId: 'status-02',
            statusName: 'Waiting for Data',
            type: 'manuell',
            locked: true
        },
        {
            statusId: 'status-03',
            statusName: 'Preflight',
            type: 'manuell',
            locked: false
        },
        {
            statusId: 'status-04',
            statusName: 'Waiting for Approval',
            type: 'manuell',
            locked: false
        },
        {
            statusId: 'status-05',
            statusName: 'Approved',
            type: 'manuell',
            locked: true
        },
        {
            statusId: 'status-06',
            statusName: 'Imposing',
            type: 'manuell',
            locked: false
        },
        {
            statusId: 'status-07',
            statusName: 'Ready for Print',
            type: 'auto',
            locked: false
        },
        {
            statusId: 'status-08',
            statusName: 'Printing',
            type: 'manuell',
            locked: false
        },
        {
            statusId: 'status-09',
            statusName: 'Finished',
            type: 'auto',
            locked: false
        },
        {
            statusId: 'error',
            statusName: 'Error',
            type: 'auto',
            locked: false
        }
    ];

    // Tools
    tools: any[] = [
        {name: 'add', active: false, disabled: false, show: true},
        {name: 'delete', active: false, disabled: false, show: true},
        {name: 'edit', active: false, disabled: false, show: true},
        {name: 'copy', active: false, disabled: false, show: true},
        {name: 'group', active: false, disabled: false, show: true},
        {name: 'export', active: false, disabled: false, show: true},
        {name: 'mail', active: false, disabled: false, show: true},
        {name: 'impose', active: false, disabled: false, show: true},
        {name: 'print', active: false, disabled: false, show: true},
        {name: 'home', active: false, disabled: false, show: false},
        {name: 'sidebar', active: true, disabled: false, show: true},
        {name: 'custom', active: false, disabled: false, show: false},
    ];

    // Collapse Tools on Mobile
    collapseTools = true;

    // Orders
    demoOrders: any[] = [
        {
            id: '0102041',
            orderName: 'Bestellung 00224',
            orderStatus: 'error',
            deadline: '05.04.2019',
            contact: 1,
            custom: [
                {id: 1, value: 1},
                {id: 2, value: 1}
            ]
        },
        {
            id: '0102042',
            orderName: 'Bestellung 00225',
            orderStatus: 'status-02',
            deadline: '06.08.2019',
            contact: 2,
            custom: [
                {id: 1, value: 2},
                {id: 2, value: 2}
            ]
        }
    ];

    // Products
    demoProducts: any[] = [
        {
            id: '000122',
            productName: 'Folder Max Mustermann',
            orderId: '0102041',
            productType: 1,
            productStatus: 'error',
            copies: '1000',
            price:  '329.00',
            contact:    1,
            preflightPreset: 1,
            custom: [
                {id: 1, value: 1},
                {id: 2, value: 1}
            ]
        },
        {
            id: '000123',
            orderId: '0102041',
            productName: 'Flyer Max Mustermann',
            productType: 2,
            productStatus: 'status-02',
            copies: '5000',
            price: '298.00',
            contact:    2,
            preflightPreset: 2,
            custom: [
                {id: 1, value: 2},
                {id: 2, value: 2}
            ]
        }
    ];

    // Files
    demoFiles: any[] = [
        {
            preview: '175877-1_0001.jpeg',
            id: '100299',
            productId: '000122',
            orderId: '0102041',
            fileName: 'Folder-VS.pdf',
            fileStatus: 'error',
            copies: '1000',
            width: 210,
            height: 297,
            pages: 1,
            substrate: 1,
            colors: '5 / 5',
            offset: 3,
            weight: 300,
            preflightPreset: 1,
            version: 2,
            fileType: 1
        },
        {
            preview: '175877-1_0002.jpeg',
            id: '100298',
            productId: '000122',
            orderId: '0102041',
            fileName: 'Folder-RS.pdf',
            fileStatus: 'status-07',
            copies: '1000',
            width: 210,
            height: 297,
            pages: 1,
            substrate: 2,
            colors: '4 / 4',
            offset: 5,
            weight: 500,
            preflightPreset: 0,
            version: 1,
            fileType: 2
        },
        {
            preview: '175877-1_0004.jpeg',
            id: '100297',
            productId: '000123',
            orderId: '0102041',
            fileName: 'Flyer.pdf',
            fileStatus: 'status-02',
            copies: '500',
            width: 210,
            height: 148,
            pages: 2,
            substrate: 3,
            colors: '4 / 4',
            offset: 3,
            weight: 500,
            preflightPreset: 2,
            version: 1,
            fileType: 2
        },
        {
            preview: '175877-1_0002.jpeg',
            id: '100296',
            productId: '000123',
            orderId: '0102042',
            fileName: 'Test-Flyer.pdf',
            fileStatus: 'status-03',
            copies: '14500',
            width: 210,
            height: 148,
            pages: 2,
            substrate: 3,
            colors: '4 / 4',
            offset: 3,
            weight: 500,
            preflightPreset: 2,
            version: 3,
            fileType: 1
        }
    ];

    // Substrates
    substrates: any[] = [
        {id: 1, name: 'Chromocard HB'},
        {id: 2, name: 'GardaGloss Art'},
        {id: 3, name: 'GardaMatt Art'},
        {id: 4, name: 'Magno Gloss'},
        {id: 5, name: 'Arctic Volume HighWite'},
        {id: 6, name: 'Arctic Volume Ivory'}
    ];

    // Preflight Presets
    preflightPresets: any[] = [
        {
            id: 1,
            name: 'Preflight Preset 01',
            rgb: true,
            numberOfPages: true,
            format: true,
            fonts: true,
            separations: true,
            tac: true,
            tacMax: 345,
            resolution: false,
            resolutionHalfStroke: 198,
            resolutionStroke: 598,
            offset: false,
            offsetValue: '0.1',
            offsetMin: '1'
        },
        {
            id: 2,
            name: 'Preflight Preset 02',
            rgb: true,
            numberOfPages: true,
            format: false,
            fonts: true,
            separations: true,
            tac: true,
            tacMax: 345,
            resolution: true,
            resolutionHalfStroke: 198,
            resolutionStroke: 598,
            offset: false,
            offsetValue: '0.1',
            offsetMin: '1'
        },
        {
            id: 3,
            name: 'Preflight Preset 03',
            rgb: true,
            numberOfPages: true,
            format: true,
            fonts: true,
            separations: true,
            tac: true,
            tacMax: 345,
            resolution: true,
            resolutionHalfStroke: 198,
            resolutionStroke: 598,
            offset: false,
            offsetValue: '0.1',
            offsetMin: '1'
        },
        {
            id: 4,
            name: 'Preflight Preset 04',
            rgb: true,
            numberOfPages: true,
            format: false,
            fonts: false,
            separations: true,
            tac: false,
            tacMax: 345,
            resolution: false,
            resolutionHalfStroke: 198,
            resolutionStroke: 598,
            offset: true,
            offsetValue: '0.1',
            offsetMin: '1'
        },
        {
            id: 5,
            name: 'Preflight Preset 05',
            rgb: false,
            numberOfPages: true,
            format: true,
            fonts: true,
            separations: true,
            tac: true,
            tacMax: 345,
            resolution: false,
            resolutionHalfStroke: 198,
            resolutionStroke: 598,
            offset: false,
            offsetValue: '0.1',
            offsetMin: '1'
        }
    ];

    // Impose Presets
    imposePresets: any[] = [
        {id: 1, name: 'Impose Preset 01', module: 0},
        {id: 2, name: 'Impose Preset 02', module: 1},
        {id: 3, name: 'Impose Preset 03', module: 2},
        {id: 4, name: 'Impose Preset 04', module: 0},
    ];

    // Color Profiles
    colorProfiles: any[] = [
        {id: 1, name: 'ISO Coated V2'},
        {id: 2, name: 'ISO Coated V2 300%'},
        {id: 3, name: 'FOGRA 27'},
        {id: 4, name: 'FOGRA 39'}
    ];

    // Workflows
    workflows: any[] = [
        {id: 1, name: 'Standard Workflow'},
        {id: 2, name: 'Folder'},
        {id: 3, name: 'Folder mit Cellophanierung'},
        {id: 4, name: 'Briefumschlag'},
        {id: 5, name: 'Visitenkarte einseitig'},
        {id: 6, name: 'Visitenkarte beidseitig'},
    ];

    // Modules
    modules: any[] = [
        {id: 1, name: 'ImpoStrip', active: false},
        {id: 2, name: 'pdfToolbox', active: true},
        {id: 3, name: 'KIM', active: false},
    ];

    // Tools (Settings)
    settingTools: any[] = [
        {id: 1, name: 'Pitstop'},
        {id: 2, name: 'Pitstop Modify'}
    ];

    // User
    user: any[] = [
        {
            id: 1,
            userName: 'stefan',
            name: 'Stefan Standard',
            firstName: 'Stefan',
            lastName: 'Standard',
            mail: 'stefan@standard.de',
            pwd: '123456',
            rights: 1,
            status: false},
        {
            id: 2,
            userName: 'peterP',
            name: 'Peter Professional',
            firstName: 'Peter',
            lastName: 'Professional',
            mail: 'peter.prefoessional@example.com',
            pwd: '123456',
            rights: 1,
            status: true},
        {
            id: 3,
            userName: 'admin',
            name: 'Armin Administrator',
            firstName: 'Armin',
            lastName: 'Administrator',
            mail: 'armin@admin.com',
            pwd: '123456',
            rights: 2,
            status: true},
        {
            id: 4,
            userName: 'ddeveloper',
            name: 'Daniel Developer',
            firstName: 'Daniel',
            lastName: 'Entwickler',
            mail: 'd.entwickler@coder.io',
            pwd: '123456',
            rights: 3,
            status: true}
    ];

    // User Rights
    userRights: any[] = [
        {id: 1, name: 'Standard'},
        {id: 2, name: 'Administrator'},
        {id: 3, name: 'Entwickler'}
    ];

    // Greyscales
    greyScales: any[] = [1, 4, 8, 64, 128, 256];

    // Custom Values
    customValues: any[] = [
        {
            id: 1,
            name: 'Custom 01',
            values: [
                {id: 1, value: 'auto'},
                {id: 2, value: 'manuell'}
            ]
        },
        {
            id: 2,
            name: 'Custom 02',
            values: [
                {id: 1, value: 'aaa'},
                {id: 2, value: 'bbb'}
            ]
        },
        {
            id: 3,
            name: 'Custom 03',
            values: [
                {id: 1, value: 'Lorem'},
                {id: 2, value: 'Ipsum'}
            ]
        },
        {
            id: 4,
            name: 'Custom 04',
            values: [
                {id: 1, value: 'Test'},
                {id: 2, value: 'Beispiel'}
            ]
        }
    ];

    // Product Types
    productTypes: any[] = [
        { id: 1, product: 'Folder'},
        { id: 2, product: 'Flyer'},
        { id: 3, product: 'Visitenkarte'},
    ];

    // Companies
    companies: any[] = [
        {
            id: 1,
            name: 'Musterfirma',
            phone: '+49 / 123 456 78',
            street: 'Musterstraße 1',
            zip: '12345',
            city: 'Musterstadt',
            country: 0
        },
        {
            id: 2,
            name: 'Example Company',
            phone: '+49 / 222 333 222',
            street: 'Examplestreet 2',
            zip: '64789',
            city: 'Example City',
            country: 2
        },
    ];

    // Company Groups
    companyGroups: any[] = [
        {id: 1, name: 'Online Druckereien', companies: [1, 2]},
        {id: 2, name: 'High End Kunden', companies: [1]},
        {id: 3, name: 'Low Budget Kunden', companies: [2]},
    ];

    // Contacts
    contacts: any[] = [
        {
            id: 1,
            firstName: 'Max',
            lastName: 'Mustermann',
            name: 'Max Mustermann',
            mail: 'max@mustermann.de',
            phone: '+49 / 1234 456 78',
            company: 1,
            role: 'Hauptkontakt',
            street: 'Beispielstraße 1',
            zip: '112233',
            city: 'Beispielstadt',
            country: 0,
            useCompanyAddress: false
        },
        {
            id: 2,
            firstName: 'Marta',
            lastName: 'Musterfrau',
            name: 'Marta Musterfrau',
            mail: 'marta@mustermann.de',
            phone: '+49 / 222 222 333',
            company: 1,
            role: 'Grafiker',
            street: '',
            zip: '',
            city: '',
            country: 0,
            useCompanyAddress: true
        },
        {
            id: 3,
            firstName: 'Edward',
            lastName: 'Example',
            name: 'Edward Example',
            mail: 'edward@example.de',
            phone: '+49 / 963 852 741',
            company: 2,
            role: 'CEO',
            street: 'West Coast Blvd.',
            zip: 'SY15661',
            city: 'Oceanside',
            country: 2,
            useCompanyAddress: false
        },
    ];

    // Contacts w/ Mail Addresses
    contactsMail = this.contacts.map(contact => {
        const temp: any = [
            {
                id: false,
                name: false
            }
        ];

        temp.id = contact.id;
        temp.name = contact.name + ' (' + contact.mail + ')';

        return temp;
    });

    // Countries
    countries: any[] = [
        {id: 1, name: 'Deutschland'},
        {id: 2, name: 'Österreich'},
        {id: 3, name: 'Schweiz'},
    ];

    // Settings
    demoSettings: any[] = [
        {id: 1, name: 'auto'},
        {id: 2, name: 'manuell'},
    ];

    constructor() { }

    /**
     * Manages the active state for toolbar tools.
     *
     * @param elem – The selected tool.
     * @param value – Enable / disable active state.
     */
    setActiveTool(elem, value): void {
        const index = this.tools.findIndex(x => x.name === elem);
        this.tools[index].active = value;
    }

    /**
     * Enables / disables the selected tool.
     *
     * @param elem – The selected tool.
     * @param value – Enable / disable disabled state.
     */
    disableTool(elem, value): void {
        const index = this.tools.findIndex(x => x.name === elem);
        this.tools[index].disabled = value;
    }

    /**
     * Shows / hides a tool.
     *
     * @param tool – The index of the selected tool in the tools array.
     * @param value – The value to be set (true/false).
     */
    hideTool(tool, value) {
        const index = this.tools.findIndex(x => x.name === tool);
        this.tools[index].show = value;
    }

    /**
     * Prevents element from closing triggered by mouse event.
     *
     * @remarks
     * This method is used to prevent dropdowns from closing.
     *
     * @param event – The mouse event
     */
    preventClose(event: MouseEvent) {
        event.stopImmediatePropagation();
    }

    /**
     * Returns the file.
     *
     * @param fileId – The file ID
     * @returns The file data as array.
     */
    getFile(fileId) {
        const index = this.demoFiles.findIndex(x => x.id === fileId);
        return this.demoFiles[index];
    }

    /**
     * Returns all files of a product.
     *
     * @param productId – The selected product.
     * @returns Array of corresponding files.
     */
    getFilesByProductId(productId) {
        const files = [];

        for (const file in this.demoFiles) {
            if (this.demoFiles[file].productId === productId) {
                files.push(this.demoFiles[file]);
            }
        }
        return files;
    }

    /**
     * Returns all demo files by file type.
     *
     * @param fileType – The selected file type.
     * @returns Array of demo files.
     */
    getDemoFilesByType(fileType) {
        let files = [];

        if (fileType === 0) {
            files = this.demoFiles;
        } else {

            for (const file in this.demoFiles) {
                if (this.demoFiles[file].fileType === fileType) {
                    files.push(this.demoFiles[file]);
                }
            }
        }

        return files;
    }

    /**
     * Returns a product.
     *
     * @param productId – The ID of the product.
     * @returns The product data as array.
     */
    getProductById(productId) {
        const index = this.demoProducts.findIndex(x => x.id === productId);
        return this.demoProducts[index];
    }

    /**
     * Returns all products of an order.
     *
     * @param orderId – The selected order.
     * @returns Array of found products.
     */
    getProductsByOrderId(orderId) {
        const products: any[] = this.demoProducts.filter(x => x.orderId === orderId);

        if (products.length > 0) {
            return products;
        } else {
            return undefined;
        }
    }

    /**
     * Returns the name of a product
     *
     * @param productId – The product ID.
     * @returns The product name as string.
     */
    getProductName(productId) {
        const index = this.demoProducts.findIndex(x => x.id === productId);
        return this.demoProducts[index].productName;
    }

    /**
     * Returns an order.
     *
     * @param orderId – The order ID.
     * @returns The order data as array.
     */
    getOrderById(orderId) {
        const index = this.demoOrders.findIndex(x => x.id === orderId);
        return this.demoOrders[index];
    }

    /**
     * Returns the name of an order.
     *
     * @param orderId – The selected order.
     * @returns The order name as string.
     */
    getOrderName(orderId) {
        const index = this.demoOrders.findIndex(x => x.id === orderId);
        return this.demoOrders[index].orderName;
    }

    /**
     * Counts the files of a product.
     *
     * @param productId – The selected product.
     * @returns The number of files.
     */
    countFiles(productId) {
        let count = 0;

        for (const file of this.demoFiles) {

            if (file.productId === productId) {
                count = count + 1;
            }
        }

        return count;
    }

    /**
     * Counts the products of an order.
     *
     * @param orderId – The selected order.
     * @returns The number of products.
     */
    countProducts(orderId) {
        let count = 0;

        for (const file of this.demoProducts) {

            if (file.orderId === orderId) {
                count = count + 1;
            }
        }

        return count;
    }

    /**
     * Counts all files of an order.
     *
     * @param orderId – The order ID.
     * @returns The number of files.
     */
    countAll(orderId) {
        let count = 0;

        for (const file of this.demoFiles) {

            if (file.orderId === orderId) {
                count = count + 1;
            }
        }

        return count;
    }

    /**
     * Returns the name of a status.
     *
     * @param statusId – The selected status.
     * @returns The status name as string.
     */
    getStatusName(statusId) {
        const currentIndex = this.statusRange.findIndex(x => x.statusId === statusId);
        return(this.statusRange[currentIndex].statusName);
    }

    /**
     * Returns the selected custom value.
     *
     * @param id – The ID of the selected custom value
     * @returns The values as array.
     */
    getCustomValue(id) {
        const index = this.customValues.findIndex(x => x.id === id);
        return this.customValues[index];
    }

    /**
     * Returns the selected substrate.
     *
     * @param id – The substrate ID.
     * @returns The substrate data array.
     */
    getSubstrateById(id) {
        const index = this.substrates.findIndex(x => x.id === id);
        return this.substrates[index];
    }

    /**
     * Return the selected contact.
     *
     * @param contactId – The ID of the selected contact.
     * @returns The contact data as array.
     */
    getContactById(contactId) {
        const index = this.contacts.findIndex(x => x.id === contactId);
        return this.contacts[index];
    }

    /**
     * Returns all contacts of a company.
     *
     * @param companyId – The selected company.
     * @returns The contacts as array.
     */
    getContactsByCompany(companyId) {
        const filteredContacts = [];

        for (const contacts of this.contacts) {

            const result: any = [
                {
                    id: false,
                    name: false
                }
            ];

            if (contacts.company === companyId) {
                result.id = contacts.id;
                result.name = contacts.name;
                filteredContacts.push(result);
            }
        }

        return filteredContacts;
    }

    /**
     * Returns the company with a specific id.
     *
     * @param companyId – The id of the selected company.
     * @returns The company data as array.
     */
    getCompanyById(companyId) {
        const company = this.companies.findIndex(x => x.id === companyId);

        if (this.companies[company]) {
            return this.companies[company];
        } else {
            return false;
        }
    }

    /**
     * Returns the companies of a group.
     *
     * @param selectedGroup – Id of the selected group.
     * @returns The companies as array.
     */
    getCompaniesByGroup(selectedGroup) {
        const filteredCompanies = [];

        selectedGroup.companies.forEach(id => {
            if (this.getCompanyById(id)) {
                filteredCompanies.push(this.getCompanyById(id));
            }
        });

        return filteredCompanies;
    }

    /**
     * Returns the contact of a product.
     * @param productId – The selected product.
     * @returns The contact data as array.
     */
    buildContact(productId) {
        let contact = false;
        let contactId = null;

        contactId = this.getProductById(productId).contact;
        contact = this.getContactById(contactId);
        return contact;
    }

    /**
     * Returns the user right with a specific id.
     *
     * @param id – The id of the user right.
     */
    getUserRight(id) {
        const userRight = this.userRights.findIndex(x => x.id === id);

        if (this.userRights[userRight]) {
            return this.userRights[userRight];
        } else {
            return false;
        }
    }
}
