import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListingFolderService {
    // Folder Scope (global or local)
    folderScope: string = 'global';

    // Folder Level (group or folder)
    folderLevel: string = 'group';


    // Global Folder Groups
    globalFolderGroups = ['Produktionstage', 'Maschinen', 'Sachbearbeiter', 'Spezialjobs'];

    // Global Folder Default Headline
    globalFolderHeadline = this.globalFolderGroups[0];

    // Global Folder
    globalFolder: any[] = [
        ['Tagesproduktion', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Eilaufträge'],
        ['Drucker 01', 'Drucker 02', 'Drucker 03'],
        ['Max Mustermann', 'Edward Example', 'Bettina Beispiel'],
        ['Exklusivmaterial', 'Sonderfarben', 'Weihnachten: Gold', 'Weihnachten: Silber']
    ];

    // Local Folder
    myFolder = ['TODO', 'Neue Aufträge nach Kunden', 'Spezialkunden', 'Unsortiert'];

    // All Folders
    globalFolderFlat = [].concat.apply([], this.globalFolder);
    allFolder = this.myFolder.concat(this.globalFolderFlat);

    // Show Folder
    public showFolder: boolean;

    // Selected Default Folder
    public selectedFolder = this.globalFolder[0];

    // Count of Selected Folder(s)
    public folderSelected: number = 0;

    // Name of Selected Folder
    public folderSelectedName: string = '';

    // Dropdown content to be displayed
    dropdownContent: string = 'new';

    constructor() { }

    /**
     * Changes between folder group and folder list.
     *
     * @param selection – The selected folder group.
     * @param index – The selected index.
     */
    public toggleFolderView(selection, index): void {
        this.showFolder = !this.showFolder;

        if (index !== undefined) {
            this.selectedFolder = this.globalFolder[index];
            this.globalFolderHeadline = this.globalFolderGroups[index];
            this.folderLevel = 'folder';
        } else {
            this.folderLevel = 'group';
        }
    }

    /**
     * Selects a folder.
     *
     * @param folder – The selected folder.
     */
    public setActiveFolder(folder) {
        if (folder.target.classList.contains('active')) {
            this.folderSelected--;
        } else {
            this.folderSelected++;
            this.folderSelectedName = folder.target.name;
        }

        folder.target.classList.toggle('active');
    }

    /**
     * Returns the name of the selected folder.
     *
     * @param scope – The folder scope ('global' or 'local').
     */
    getFolderName(scope) {
        if (this.folderSelected > 1) {
            return 'multiple';
        } else {
            return false;
        }
    }

    /**
     * Resets the folder scope, adjusts scope & folder level.
     * @param event – The change event.
     */
    public resetFolderScope(event) {
        // Deselect all Folder on Tab Change
        const folder = document.querySelectorAll('.folder');

        for(const fd of folder as any) {
            fd.classList.remove('active');
        }

        // Reset Folder Count
        this.folderSelected = 0;

        // Reset Dropdown Content
        this.dropdownContent = 'new';

        // Change Scope
        if (event.index === 0) {
            this.folderScope = 'global';
        } else {
            this.folderScope = 'local';
            this.folderLevel = 'folder';
        }
    }
}
