import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './navigation-main/navigation.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

import {
    BsLocaleService,
    defineLocale,
    deLocale
} from 'ngx-bootstrap';

import {
    BsDropdownModule,
    CollapseModule,
    BsDatepickerModule,
    PaginationModule,
    SortableModule,
} from 'ngx-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MatTooltipModule,
    MatSidenavModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatDialogModule,
    MatListModule,
    MAT_RIPPLE_GLOBAL_OPTIONS,
    RippleGlobalOptions,
    MatCheckboxModule,
    MatRadioModule,
    MatSlideToggleModule
} from '@angular/material';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelectModule} from '@ng-select/ng-select';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarFolderComponent } from './sidebar/sidebar-folder/sidebar-folder.component';
import { FolderToolsComponent } from './sidebar/sidebar-folder/folder-tools/folder-tools.component';
import { FolderGlobalComponent } from './sidebar/sidebar-folder/folder-global/folder-global.component';
import { FolderLocalComponent } from './sidebar/sidebar-folder/folder-local/folder-local.component';
import { SidebarPreviewComponent } from './sidebar/sidebar-preview/sidebar-preview.component';
import { SidebarService } from '../services/sidebar.service';
import { SidebarEditComponent } from './sidebar/sidebar-edit/sidebar-edit.component';
import { SendComponent } from './dropdown/send/send.component';
import { ImposeComponent } from './dropdown/impose/impose.component';
import { DemoDataService } from '../services/demo-data.service';
import { PrintComponent } from './dropdown/print/print.component';
import { ExportComponent } from './dropdown/export/export.component';
import { GroupComponent } from './dropdown/group/group.component';
import { ModalGroupNewComponent } from './sidebar/sidebar-folder/modal-group-new/modal-group-new.component';
import { ModalGroupEditComponent } from './sidebar/sidebar-folder/modal-group-edit/modal-group-edit.component';
import { ModalFolderNewComponent } from './sidebar/sidebar-folder/modal-folder-new/modal-folder-new.component';
import { ModalFolderEditComponent } from './sidebar/sidebar-folder/modal-folder-edit/modal-folder-edit.component';
import { EditFileComponent } from './sidebar/sidebar-edit/edit-file/edit-file.component';
import { EditOrderComponent } from './sidebar/sidebar-edit/edit-order/edit-order.component';
import { EditProductComponent } from './sidebar/sidebar-edit/edit-product/edit-product.component';
import { PreviewFileComponent } from './sidebar/sidebar-preview/preview-file/preview-file.component';
import { PreviewProductComponent } from './sidebar/sidebar-preview/preview-product/preview-product.component';
import { PreviewOrderComponent } from './sidebar/sidebar-preview/preview-order/preview-order.component';
import { NewFileComponent } from './sidebar/sidebar-new/new-file/new-file.component';
import { SidebarNewComponent } from './sidebar/sidebar-new/sidebar-new.component';
import { NewProductComponent } from './sidebar/sidebar-new/new-product/new-product.component';
import { NewOrderComponent } from './sidebar/sidebar-new/new-order/new-order.component';
import { SidebarPreflightComponent } from './sidebar/sidebar-preflight/sidebar-preflight.component';
import { PreflightSendComponent } from './sidebar/sidebar-preflight/preflight-send/preflight-send.component';
import { ConfirmDeleteComponent } from './modal/confirm-delete/confirm-delete.component';
import { PreflightEditComponent } from './sidebar/sidebar-preflight/preflight-edit/preflight-edit.component';
import { SidebarToolsComponent } from './sidebar/sidebar-tools/sidebar-tools.component';
import { CustomValuesComponent } from './dropdown/custom-values/custom-values.component';
import { QuicknavComponent } from './toolbar/quicknav/quicknav.component';

// Disable Ripple for all Material UI Elements
const globalRippleConfig: RippleGlobalOptions = {
    disabled: true
};

// Language Locale for Datepicker
defineLocale('de', deLocale);

// Default Config for Scrollbars
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: false
};

@NgModule({
  declarations: [
      NavigationComponent,
      ToolbarComponent,
      SidebarComponent,
      SidebarFolderComponent,
      FolderToolsComponent,
      FolderGlobalComponent,
      FolderLocalComponent,
      SidebarPreviewComponent,
      SidebarEditComponent,
      SendComponent,
      ImposeComponent,
      PrintComponent,
      ExportComponent,
      GroupComponent,
      ModalGroupNewComponent,
      ModalGroupEditComponent,
      ModalFolderNewComponent,
      ModalFolderEditComponent,
      EditFileComponent,
      EditOrderComponent,
      EditProductComponent,
      PreviewFileComponent,
      PreviewProductComponent,
      PreviewOrderComponent,
      NewFileComponent,
      SidebarNewComponent,
      NewProductComponent,
      NewOrderComponent,
      SidebarPreflightComponent,
      PreflightSendComponent,
      ConfirmDeleteComponent,
      PreflightEditComponent,
      SidebarToolsComponent,
      CustomValuesComponent,
      QuicknavComponent
  ],
  entryComponents: [ ConfirmDeleteComponent],
  imports: [
    CommonModule,
      BsDropdownModule.forRoot(),
      BsDatepickerModule.forRoot(),
      CollapseModule.forRoot(),
      CommonModule,
      PaginationModule.forRoot(),
      SortableModule.forRoot(),
      BrowserAnimationsModule,
      FormsModule,
      ReactiveFormsModule,
      MatTooltipModule,
      MatSidenavModule,
      MatTabsModule,
      MatTableModule,
      MatPaginatorModule,
      MatSortModule,
      MatFormFieldModule,
      MatInputModule,
      MatRadioModule,
      MatCheckboxModule,
      MatSelectModule,
      MatExpansionModule,
      MatSnackBarModule,
      MatDialogModule,
      MatListModule,
      MatSlideToggleModule,
      NgSelectModule,
      NgxDropzoneModule,
      PerfectScrollbarModule
  ],
  providers: [
      {provide: MAT_RIPPLE_GLOBAL_OPTIONS, useValue: globalRippleConfig},
      SidebarService,
      DemoDataService,
      {provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG}
  ],
  exports: [
      NavigationComponent,
      ToolbarComponent,
      SidebarComponent,
      SidebarPreviewComponent,
      SidebarEditComponent,
      SidebarNewComponent,
      SidebarPreflightComponent,
      SidebarToolsComponent,
      PreviewOrderComponent,
      PreviewProductComponent,
      EditOrderComponent,
      EditProductComponent,
      BsDropdownModule,
      BsDatepickerModule,
      FormsModule,
      ReactiveFormsModule,
      MatSidenavModule,
      MatTabsModule,
      MatTableModule,
      MatFormFieldModule,
      MatInputModule,
      MatRadioModule,
      MatSortModule,
      MatPaginatorModule,
      MatCheckboxModule,
      MatSelectModule,
      MatExpansionModule,
      MatSnackBarModule,
      MatDialogModule,
      MatListModule,
      MatTooltipModule,
      MatSlideToggleModule,
      PaginationModule,
      NgSelectModule,
      NgxDropzoneModule,
      SortableModule,
      DragDropModule,
      PerfectScrollbarModule
  ]
})
export class LayoutModule {
    constructor(public localeService: BsLocaleService) {
        localeService.use('de');
    }
}
