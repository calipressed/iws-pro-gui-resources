import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ListingService } from '../../../services/listing.service';

@Component({
  selector: 'pt-sidebar-preview',
  templateUrl: './sidebar-preview.component.html',
  styleUrls: ['./sidebar-preview.component.scss']
})
export class SidebarPreviewComponent implements OnInit {
    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }

    get demo(): DemoDataService {
        return this.demoData;
    }

    set demo(demo: DemoDataService) {
        this.demoData = demo;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService,
    ) { }

    ngOnInit() {
    }
}
