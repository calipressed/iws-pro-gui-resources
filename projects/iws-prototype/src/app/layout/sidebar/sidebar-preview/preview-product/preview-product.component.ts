import { Component, OnInit, Input } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { ListingService } from '../../../../services/listing.service';
import {DetailsService} from '../../../../services/details.service';

@Component({
  selector: 'pt-preview-product',
  templateUrl: './preview-product.component.html',
  styleUrls: ['./preview-product.component.scss']
})
export class PreviewProductComponent implements OnInit {
    // Product is embedded on details page
    @Input() isDetails: boolean;

    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }


    get data(): DemoDataService {
        return this.demoData;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get details(): DetailsService {
        return this.detailsService;
    }

    set details(data: DetailsService) {
        this.detailsService = data;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService,
        public detailsService: DetailsService
    ) {}

  ngOnInit() {
    // Initial Product
    this.listingService.currentProduct = this.demoData.getProductById('000122');
    this.listingService.selectedOrder = this.demoData.getOrderById('0102041');
    this.listingService.currentFiles = this.demoData.getFilesByProductId('000122');
  }

}
