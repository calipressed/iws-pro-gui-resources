import { Component, OnInit, Input } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { ListingService } from '../../../../services/listing.service';
import { DetailsService } from '../../../../services/details.service';

@Component({
  selector: 'pt-preview-order',
  templateUrl: './preview-order.component.html',
  styleUrls: ['./preview-order.component.scss']
})
export class PreviewOrderComponent implements OnInit {
    // Order is embedded on details page
    @Input() isDetails: boolean;

    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }


    get data(): DemoDataService {
        return this.demoData;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get details(): DetailsService {
        return this.detailsService;
    }

    set details(data: DetailsService) {
        this.detailsService = data;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService,
        public detailsService: DetailsService
    ) {}

  ngOnInit() {
      this.listingService.activeOrder = this.demoData.getOrderById('0102041');
  }

}
