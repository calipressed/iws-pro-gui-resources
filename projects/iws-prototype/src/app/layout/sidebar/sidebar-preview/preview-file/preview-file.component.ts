import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { ListingService } from '../../../../services/listing.service';

@Component({
  selector: 'pt-preview-file',
  templateUrl: './preview-file.component.html',
  styleUrls: ['./preview-file.component.scss']
})
export class PreviewFileComponent implements OnInit {

    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }


    get data(): DemoDataService {
        return this.demoData;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService
    ) {}

    ngOnInit() {
        // Initial Product
        this.listingService.currentFile = this.demoData.getFile('100299');
        this.listingService.currentProduct = this.demoData.getProductById('000122');
        this.listingService.currentOrder = this.demoData.getOrderById('0102041');
        this.listingService.currentProducts = this.demoData.getProductsByOrderId('0102041');
    }

}
