import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { ListingService } from '../../../../services/listing.service';
import { SidebarService } from '../../../../services/sidebar.service';
import { ListingFolderService } from '../../../../services/listing-folder.service';

@Component({
  selector: 'pt-edit-file',
  templateUrl: './edit-file.component.html',
  styleUrls: ['./edit-file.component.scss']
})
export class EditFileComponent implements OnInit {

    get data(): DemoDataService {
        return this.demoData;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }

    get sidebar(): SidebarService {
        return this.sidebarService;
    }

    set sidebar(data: SidebarService) {
        this.sidebarService = data;
    }

    get folder(): ListingFolderService {
        return this.folderService;
    }

    set folder(data: ListingFolderService) {
        this.folderService = data;
    }

    constructor(
        public demoData: DemoDataService,
        public listingService: ListingService,
        public sidebarService: SidebarService,
        public folderService: ListingFolderService
    ) { }

    editOrder: any[];
    editCompany: any[];
    editContact: any[];

    ngOnInit() {
      this.editOrder = this.listingService.currentFile['orderId'];
      this.editCompany = this.demoData.getCompanyById(this.listingService.currentProduct['contact']);
      this.editContact = this.demoData.getContactsByCompany(this.editCompany['id']);
  }

}
