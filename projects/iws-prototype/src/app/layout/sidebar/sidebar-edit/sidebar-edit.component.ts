import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ListingService } from '../../../services/listing.service';

@Component({
  selector: 'pt-sidebar-edit',
  templateUrl: './sidebar-edit.component.html',
  styleUrls: ['./sidebar-edit.component.scss']
})

export class SidebarEditComponent implements OnInit {
    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }

    get demo(): DemoDataService {
        return this.demoData;
    }

    set demo(demo: DemoDataService) {
        this.demoData = demo;
    }

    constructor(
        public demoData: DemoDataService,
        public listingService: ListingService
    ) { }

    ngOnInit() {
    }
}
