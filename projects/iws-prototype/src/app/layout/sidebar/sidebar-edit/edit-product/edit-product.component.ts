import {Component, Input, OnInit} from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { SidebarService } from '../../../../services/sidebar.service';
import { ListingFolderService } from '../../../../services/listing-folder.service';
import { ListingService } from '../../../../services/listing.service';
import { DetailsService } from '../../../../services/details.service';

@Component({
  selector: 'pt-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  // Edit order is opened on details page
  @Input() isDetails: boolean;

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get sidebar(): SidebarService {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get folder(): ListingFolderService {
    return this.folderService;
  }

  set folder(data: ListingFolderService) {
    this.folderService = data;
  }

  get details(): DetailsService {
    return this.detailsService;
  }

  set details(data: DetailsService) {
    this.detailsService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public sidebarService: SidebarService,
      public listingService: ListingService,
      public folderService: ListingFolderService,
      public detailsService: DetailsService
  ) { }

  ngOnInit() {
  }

}
