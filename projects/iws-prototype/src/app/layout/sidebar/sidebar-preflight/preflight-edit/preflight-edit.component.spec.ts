import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreflightEditComponent } from './preflight-edit.component';

describe('PreflightEditComponent', () => {
  let component: PreflightEditComponent;
  let fixture: ComponentFixture<PreflightEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreflightEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreflightEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
