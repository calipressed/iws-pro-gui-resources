import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pt-preflight-edit',
  templateUrl: './preflight-edit.component.html',
  styleUrls: ['./preflight-edit.component.scss']
})
export class PreflightEditComponent implements OnInit {

  // Enable / disable inputs
  tacMaximum = true;
  resolution = true;
  spacingDefault = true;

  constructor() { }

  ngOnInit() {
  }

}
