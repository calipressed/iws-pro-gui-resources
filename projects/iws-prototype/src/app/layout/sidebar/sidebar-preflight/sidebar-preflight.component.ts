import { Component, Input, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-sidebar-preflight',
  templateUrl: './sidebar-preflight.component.html',
  styleUrls: ['./sidebar-preflight.component.scss']
})
export class SidebarPreflightComponent implements OnInit {

  // Report is embedded on details page
  @Input() isDetails: boolean;

  get demo(): DemoDataService {
    return this.demoData;
  }

  set demo(demo: DemoDataService) {
    this.demoData = demo;
  }

  constructor(
      public demoData: DemoDataService
  ) { }

  ngOnInit() {
  }

}
