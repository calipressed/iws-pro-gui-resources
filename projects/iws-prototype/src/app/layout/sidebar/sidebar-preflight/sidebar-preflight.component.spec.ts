import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarPreflightComponent } from './sidebar-preflight.component';

describe('SidebarPreflightComponent', () => {
  let component: SidebarPreflightComponent;
  let fixture: ComponentFixture<SidebarPreflightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarPreflightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarPreflightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
