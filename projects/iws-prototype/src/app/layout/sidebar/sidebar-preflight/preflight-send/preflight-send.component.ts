import { Component, OnInit, Input } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'pt-preflight-send',
  templateUrl: './preflight-send.component.html',
  styleUrls: ['./preflight-send.component.scss']
})
export class PreflightSendComponent implements OnInit {

  // Modal is embedded on details page
  @Input() isDetails: boolean;

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get modal(): ModalService {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }


  constructor(
      public demoData: DemoDataService,
      public modalService: ModalService
  ) { }

  ngOnInit() {
  }

}
