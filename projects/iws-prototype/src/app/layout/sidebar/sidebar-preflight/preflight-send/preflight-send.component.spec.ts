import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreflightSendComponent } from './preflight-send.component';

describe('PreflightSendComponent', () => {
  let component: PreflightSendComponent;
  let fixture: ComponentFixture<PreflightSendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreflightSendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreflightSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
