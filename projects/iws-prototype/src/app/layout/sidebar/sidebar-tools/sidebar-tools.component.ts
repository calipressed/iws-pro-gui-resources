import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../services/listing.service';
import {DemoDataService} from '../../../services/demo-data.service';

@Component({
  selector: 'pt-sidebar-tools',
  templateUrl: './sidebar-tools.component.html',
  styleUrls: ['./sidebar-tools.component.scss']
})
export class SidebarToolsComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public listingService: ListingService
  ) { }

  ngOnInit() {
    // Active File
    this.listingService.currentFile =  this.demoData.demoFiles[0];

    // Get Order Data
    this.listingService.activeOrder = this.demoData.getOrderById(this.listingService.currentFile['orderId']);
    this.listingService.activeProducts = this.demoData.getProductsByOrderId(this.listingService.activeOrder['id']);
    this.listingService.orderDeadline = new Date(this.listingService.activeOrder['deadline']);
  }

}
