import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderLocalComponent } from './folder-local.component';

describe('FolderLocalComponent', () => {
  let component: FolderLocalComponent;
  let fixture: ComponentFixture<FolderLocalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderLocalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderLocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
