import { Component, OnInit } from '@angular/core';
import {ListingFolderService} from '../../../../services/listing-folder.service';

@Component({
  selector: 'pt-folder-local',
  templateUrl: './folder-local.component.html',
  styleUrls: ['./folder-local.component.scss']
})
export class FolderLocalComponent implements OnInit {

    get data() {
        return this.folderService;
    }

    constructor( public folderService: ListingFolderService) { }

  ngOnInit() {
  }

}
