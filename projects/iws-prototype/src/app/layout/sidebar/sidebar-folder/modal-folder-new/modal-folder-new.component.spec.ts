import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFolderNewComponent } from './modal-folder-new.component';

describe('ModalFolderNewComponent', () => {
  let component: ModalFolderNewComponent;
  let fixture: ComponentFixture<ModalFolderNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFolderNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFolderNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
