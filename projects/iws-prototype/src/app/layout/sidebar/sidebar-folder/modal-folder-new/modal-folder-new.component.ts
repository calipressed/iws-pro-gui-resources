import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDropdownDirective } from 'ngx-bootstrap';
import { ListingFolderService } from '../../../../services/listing-folder.service';
import { DemoDataService } from '../../../../services/demo-data.service';
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'pt-modal-folder-new',
  templateUrl: './modal-folder-new.component.html',
  styleUrls: ['./modal-folder-new.component.scss']
})
export class ModalFolderNewComponent implements OnInit {

    get data(): DemoDataService {
        return this.demoData;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get folder(): ListingFolderService {
        return this.folderData;
    }

    set folder(data: ListingFolderService) {
        this.folderData = data;
    }

    get modal(): ModalService {
        return this.modalService;
    }

    set modal(data: ModalService) {
        this.modalService = data;
    }

    // BsDropdownConfig to prevent datepicker from closing the dropdown
    @ViewChild('modFolderNew') public modFolder: BsDropdownDirective;

    // New Folder is Smartfolder
    smartFolder = false;

    constructor(
        public demoData: DemoDataService,
        public folderData: ListingFolderService,
        public modalService: ModalService) {
    }

  ngOnInit() {
  }

}
