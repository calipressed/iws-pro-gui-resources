import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalGroupEditComponent } from './modal-group-edit.component';

describe('ModalGroupEditComponent', () => {
  let component: ModalGroupEditComponent;
  let fixture: ComponentFixture<ModalGroupEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalGroupEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGroupEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
