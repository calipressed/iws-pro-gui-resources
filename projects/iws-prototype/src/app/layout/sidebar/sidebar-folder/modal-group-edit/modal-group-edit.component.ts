import { Component, OnInit } from '@angular/core';
import { ListingFolderService } from '../../../../services/listing-folder.service';
import { DemoDataService } from '../../../../services/demo-data.service';

@Component({
  selector: 'pt-modal-group-edit',
  templateUrl: './modal-group-edit.component.html',
  styleUrls: ['./modal-group-edit.component.scss']
})
export class ModalGroupEditComponent implements OnInit {

    get data(): DemoDataService {
        return this.demoData;
    }

    set data(data: DemoDataService) {
        this.demoData = data;
    }

    get folder(): ListingFolderService {
        return this.folderData;
    }

    set folder(data: ListingFolderService) {
        this.folderData = data;
    }

    constructor(
        public demoData: DemoDataService,
        public folderData: ListingFolderService
    ) {}

    ngOnInit() {
    }
}
