import { Component, OnInit } from '@angular/core';
import { ListingFolderService } from '../../../../services/listing-folder.service';

@Component({
  selector: 'pt-folder-global',
  templateUrl: './folder-global.component.html',
  styleUrls: ['./folder-global.component.scss']
})
export class FolderGlobalComponent implements OnInit {

  get data() {
      return this.folderService;
  }

  constructor( public folderService: ListingFolderService) { }

  ngOnInit() {
  }

}
