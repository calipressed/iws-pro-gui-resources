import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderGlobalComponent } from './folder-global.component';

describe('FolderGlobalComponent', () => {
  let component: FolderGlobalComponent;
  let fixture: ComponentFixture<FolderGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
