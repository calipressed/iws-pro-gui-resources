import { Component, OnInit } from '@angular/core';
import { ListingFolderService } from '../../../services/listing-folder.service';

@Component({
  selector: 'pt-sidebar-folder',
  templateUrl: './sidebar-folder.component.html',
  styleUrls: ['./sidebar-folder.component.scss']
})
export class SidebarFolderComponent implements OnInit {

    get folder(): ListingFolderService {
        return this.folderData;
    }

    set folder(data: ListingFolderService) {
        this.folderData = data;
    }

    constructor( public folderData: ListingFolderService ) { }

  ngOnInit() {
  }
}
