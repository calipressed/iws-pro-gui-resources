import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalGroupNewComponent } from './modal-group-new.component';

describe('ModalGroupNewComponent', () => {
  let component: ModalGroupNewComponent;
  let fixture: ComponentFixture<ModalGroupNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalGroupNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGroupNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
