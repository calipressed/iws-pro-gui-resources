import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFolderEditComponent } from './modal-folder-edit.component';

describe('ModalFolderEditComponent', () => {
  let component: ModalFolderEditComponent;
  let fixture: ComponentFixture<ModalFolderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFolderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFolderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
