import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FolderToolsComponent } from './folder-tools.component';

describe('FolderToolsComponent', () => {
  let component: FolderToolsComponent;
  let fixture: ComponentFixture<FolderToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FolderToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FolderToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
