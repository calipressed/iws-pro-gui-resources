import { Component, OnInit } from '@angular/core';
import { ListingFolderService } from '../../../../services/listing-folder.service';
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'pt-folder-tools',
  templateUrl: './folder-tools.component.html',
  styleUrls: ['./folder-tools.component.scss']
})
export class FolderToolsComponent implements OnInit {

    public deleteMessage;

    get folder(): ListingFolderService {
        return this.folderData;
    }

    set folder(data: ListingFolderService) {
        this.folderData = data;
    }

    get modal(): ModalService {
        return this.modalService;
    }

    set modal(data: ModalService) {
        this.modalService = data;
    }

    constructor(
        public folderData: ListingFolderService,
        public modalService: ModalService
    ) { }

    /**
     * Returns the delete message for folders or groups.
     */
   getDeleteMessage() {
       if (this.folderData.folderScope === 'global' && this.folderData.folderSelected === 0) {
           this.deleteMessage = 'Möchten Sie diese Gruppe wirklich löschen?';
       } else {
           this.deleteMessage = 'Möchten Sie diesen Ordner wirklich löschen?';
       }
   }

  ngOnInit() {
  }

}
