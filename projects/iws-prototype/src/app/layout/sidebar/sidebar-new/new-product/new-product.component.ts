import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { SidebarService } from '../../../../services/sidebar.service';
import { ListingFolderService } from '../../../../services/listing-folder.service';
import { ListingService } from '../../../../services/listing.service';

@Component({
  selector: 'pt-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get sidebar(): SidebarService {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }


  get folder(): ListingFolderService {
    return this.folderService;
  }

  set folder(data: ListingFolderService) {
    this.folderService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public sidebarService: SidebarService,
      public listingService: ListingService,
      public folderService: ListingFolderService
  ) { }

  // Array of Custom Values
  customValues: any[] = [];

  // Name of the Current Custom Value
  customValueName: string;

  // Value of the Current Custom Value
  customValueValue: string;

  // Selected Entry
  selectedEntry = -1;

  /**
   * Adds a new custom value to the array.
   */
  addValue() {
    const temp: any[] = [];
    temp[0] = this.customValueName;
    temp[1] = this.customValueValue;
    this.customValues.push(temp);

    this.customValueName = '';
    this.customValueValue = '';
  }

  /**
   * Edits the selected custom value.
   *
   * @param name – The name of the custom value.
   * @param value – The value of the custom value.
   * @param index – The index of the selected value.
   */
  editValue(name, value, index) {
    this.selectedEntry = index;
    this.customValueName = name;
    this.customValueValue = value;
  }

  /**
   * Deletes a custom value.
   * @param index – Index of the value to be deleted.
   */
  deleteValue(index) {
    this.customValues.splice(index, 1);
  }

  /**
   * Saves the data of the selected value.
   */
  saveValue() {
    this.customValues[this.selectedEntry][0] = this.customValueName;
    this.customValues[this.selectedEntry][1] = this.customValueValue;
    this.selectedEntry = -1;
  }

  ngOnInit() {
  }

}
