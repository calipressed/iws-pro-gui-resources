import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { SidebarService } from '../../../../services/sidebar.service';
import { ListingFolderService } from '../../../../services/listing-folder.service';
import { ListingService } from '../../../../services/listing.service';

@Component({
  selector: 'pt-new-file',
  templateUrl: './new-file.component.html',
  styleUrls: ['./new-file.component.scss']
})
export class NewFileComponent implements OnInit {
  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get sidebar(): SidebarService {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get folder(): ListingFolderService {
    return this.folderService;
  }

  set folder(data: ListingFolderService) {
    this.folderService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public sidebarService: SidebarService,
      public listingService: ListingService,
      public folderService: ListingFolderService
  ) { }

  ngOnInit() {
  }

}
