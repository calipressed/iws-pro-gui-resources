import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImposeComponent } from './impose.component';

describe('ImposeComponent', () => {
  let component: ImposeComponent;
  let fixture: ComponentFixture<ImposeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImposeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
