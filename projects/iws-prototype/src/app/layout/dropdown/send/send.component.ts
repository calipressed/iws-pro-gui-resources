import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ListingService } from '../../../services/listing.service';
import { ModalService } from '../../../services/modal.service';

@Component({
  selector: 'pt-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.scss']
})
export class SendComponent implements OnInit {

  get demo(): DemoDataService {
      return this.demoData;
  }

  set demo(data: DemoDataService) {
      this.demoData = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get modal(): ModalService {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public listingService: ListingService,
      public modalService: ModalService
  ) {}

  ngOnInit() {
  }

}
