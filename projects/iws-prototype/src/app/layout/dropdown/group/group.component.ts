import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  constructor( public demoData: DemoDataService) { }

  // Get Default Order Data
  selectedOrder = this.demoData.demoOrders[0].id;

  // Get Default Products
  products = this.demoData.getProductsByOrderId(this.selectedOrder);

  /**
   * Gets products when order is changed.
   *
   * @param selectedOrder – The selected order.
   */
  changeProducts(selectedOrder) {
    this.selectedOrder = selectedOrder;
    this.products = this.demoData.getProductsByOrderId(selectedOrder);
  }

  ngOnInit() {
  }

}
