import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomValuesComponent } from './custom-values.component';

describe('CustomValuesComponent', () => {
  let component: CustomValuesComponent;
  let fixture: ComponentFixture<CustomValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
