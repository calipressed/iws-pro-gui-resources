import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-custom-values',
  templateUrl: './custom-values.component.html',
  styleUrls: ['./custom-values.component.scss']
})
export class CustomValuesComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  constructor(public demoData: DemoDataService) { }

  // Export Types
  addToSelection: string[] = ['Bestellung', 'Produkt'];
  selectedType = 'Bestellung';

  ngOnInit() {
  }

}
