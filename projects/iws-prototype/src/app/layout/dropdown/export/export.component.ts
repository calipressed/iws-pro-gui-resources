import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss']
})
export class ExportComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  constructor( public demoData: DemoDataService) { }

  // Export Types
  exportTypes: string[] = ['CSV-Datei', 'Excel', 'PDF'];
  selectedType = 'CSV-Datei';

  ngOnInit() {
  }

}
