import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { DemoDataService } from '../../services/demo-data.service';
import { ListingService } from '../../services/listing.service';
import { ModalService } from '../../services/modal.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'pt-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

    public deleteMessage;

    get data() {
        return this.sidebar;
    }

    get demo(): DemoDataService {
        return this.demoData;
    }

    set demo(data: DemoDataService) {
        this.demoData = data;
    }

    get listing(): ListingService {
        return this.listingService;
    }

    set listing(data: ListingService) {
        this.listingService = data;
    }

    get modal(): ModalService {
        return this.modalService;
    }

    set modal(data: ModalService) {
        this.modalService = data;
    }

    get settings() {
        return this.settingsService;
    }

    set settings(data: SettingsService) {
        this.settingsService = data;
    }

    constructor(
        public sidebar: SidebarService,
        public demoData: DemoDataService,
        public listingService: ListingService,
        public modalService: ModalService,
        public settingsService: SettingsService
    ) { }

    // Collapse Toolbar on Mobile Devices
    isCollapsed = true;

    // Collapse Search on Mobile Devices
    mobileSearch = true;

    /**
     * Returns the delete message for files, products or orders.
     */
    getDeleteMessage() {
        switch (this.listingService.activeView) {
            case 'files':
                this.deleteMessage = 'Möchten Sie die ausgewählten Dateien wirklich löschen?';
                break;
            case 'products':
                this.deleteMessage = 'Möchten Sie die ausgewählten Produkte wirklich löschen?';
                break;
            case 'orders':
                this.deleteMessage = 'Möchten Sie die ausgewählten Bestellungen wirklich löschen?';
                break;
        }
    }

    ngOnInit() {
    }
}
