import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ListingService } from '../../../services/listing.service';

@Component({
  selector: 'pt-quicknav',
  templateUrl: './quicknav.component.html',
  styleUrls: ['./quicknav.component.scss']
})
export class QuicknavComponent implements OnInit {

  get demo(): DemoDataService {
    return this.demoData;
  }

  set demo(data: DemoDataService) {
    this.demoData = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public listingService: ListingService
  ) { }

  ngOnInit() {
  }

}
