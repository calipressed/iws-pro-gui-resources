import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'pt-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss']
})

export class ConfirmDeleteComponent implements OnInit {

  constructor(
      public dialogRef: MatDialogRef<ConfirmDeleteComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {}

  confirmDelete(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
