import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../services/login.service';

@Component({
  selector: 'pt-pwd-forgot',
  templateUrl: './pwd-forgot.component.html',
  styleUrls: ['./pwd-forgot.component.scss']
})
export class PwdForgotComponent implements OnInit {

  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  constructor( public loginService: LoginService) { }

  ngOnInit() {
  }

}
