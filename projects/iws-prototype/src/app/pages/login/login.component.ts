import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../services/login.service';

@Component({
  selector: 'pt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  constructor( public loginService: LoginService) { }

  ngOnInit() {
    // Hide Main Nav & Logout Button
    this.loginService.showMainNav = false;
    this.loginService.showLogout = false;
  }

}
