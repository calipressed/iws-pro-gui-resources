import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../services/login.service';

@Component({
  selector: 'pt-pwd-send',
  templateUrl: './pwd-send.component.html',
  styleUrls: ['./pwd-send.component.scss']
})
export class PwdSendComponent implements OnInit {

  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  constructor( public loginService: LoginService) { }

  ngOnInit() {
  }

}
