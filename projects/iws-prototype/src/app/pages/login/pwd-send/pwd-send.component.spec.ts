import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwdSendComponent } from './pwd-send.component';

describe('PwdSendComponent', () => {
  let component: PwdSendComponent;
  let fixture: ComponentFixture<PwdSendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PwdSendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwdSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
