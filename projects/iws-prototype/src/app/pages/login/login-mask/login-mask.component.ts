import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../services/login.service';

@Component({
  selector: 'pt-login-mask',
  templateUrl: './login-mask.component.html',
  styleUrls: ['./login-mask.component.scss']
})
export class LoginMaskComponent implements OnInit {

  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  constructor( public loginService: LoginService) { }

  ngOnInit() {
  }

}
