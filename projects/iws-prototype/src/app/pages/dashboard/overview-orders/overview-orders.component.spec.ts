import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewOrdersComponent } from './overview-orders.component';

describe('OverviewOrdersComponent', () => {
  let component: OverviewOrdersComponent;
  let fixture: ComponentFixture<OverviewOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
