import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-overview-orders',
  templateUrl: './overview-orders.component.html',
  styleUrls: ['./overview-orders.component.scss']
})
export class OverviewOrdersComponent implements OnInit {


  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  // Dummy Data: Order Count & Trend
  orderCountTemp: any [] = [
    { count: 8, trend: 'up'},
    { count: 12, trend: 'up'},
    { count: 7, trend: 'down'},
    { count: 4, trend: 'up'},
    { count: 6, trend: 'up'},
    { count: 11, trend: 'up'},
    { count: 17, trend: 'up'},
    { count: 3, trend: 'up'},
    { count: 5, trend: 'up'},
    { count: 7, trend: 'down'}
  ];

  constructor( public demoData: DemoDataService ) { }
  ngOnInit() {
  }

}
