import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../services/demo-data.service';
import { SidebarService } from '../../services/sidebar.service';
import { LoginService } from '../../services/login.service';
import {ListingService} from '../../services/listing.service';

@Component({
  selector: 'pt-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get sidebar(): SidebarService {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  // Manage Visibility for Vertical Tabs
  showFileOverview = true;
  showProductOverview = false;
  showOrderOverview = false;

  constructor(
      public demoData: DemoDataService,
      public sidebarService: SidebarService,
      public listingService: ListingService,
      public loginService: LoginService
  ) { }

  ngOnInit() {
    // Set Active Navigation
    this.loginService.setActivePage('Übersicht', true);
    this.loginService.setActivePage('Jobs', false);

    // Hide Tools
    this.demoData.hideTool('add', false);
    this.demoData.hideTool('delete', false);
    this.demoData.hideTool('edit', false);
    this.demoData.hideTool('copy', false);
    this.demoData.hideTool('group', false);
    this.demoData.hideTool('impose', false);
    this.demoData.hideTool('print', false);

    // Activate Tools
    this.demoData.disableTool('mail', true);

    // Hide Sidebar toggle
    this.sidebarService.hasSidebar = false;

    // Prevent Tools from Collapsing on Mobile
    this.demoData.collapseTools = false;

    // Set Active Page for Dialogs (e.g. Send)
    this.listingService.activePage = 'dashboard';
  }

}
