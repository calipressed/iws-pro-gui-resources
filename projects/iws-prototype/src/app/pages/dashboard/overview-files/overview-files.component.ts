import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-overview-files',
  templateUrl: './overview-files.component.html',
  styleUrls: ['./overview-files.component.scss']
})
export class OverviewFilesComponent implements OnInit {
  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  // Dummy Data: File Count & Trend
  fileCountTemp: any [] = [
    { count: 12, trend: 'up'},
    { count: 55, trend: 'up'},
    { count: 53, trend: 'up'},
    { count: 84, trend: 'down'},
    { count: 26, trend: 'up'},
    { count: 6, trend: 'down'},
    { count: 81, trend: 'up'},
    { count: 67, trend: 'up'},
    { count: 84, trend: 'up'},
    { count: 21, trend: 'down'}
  ];

  constructor( public demoData: DemoDataService ) { }

  ngOnInit() {
  }

}
