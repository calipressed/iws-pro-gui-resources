import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewFilesComponent } from './overview-files.component';

describe('OverviewFilesComponent', () => {
  let component: OverviewFilesComponent;
  let fixture: ComponentFixture<OverviewFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
