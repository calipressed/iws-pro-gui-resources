import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-overview-products',
  templateUrl: './overview-products.component.html',
  styleUrls: ['./overview-products.component.scss']
})
export class OverviewProductsComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  // Dummy Data: Product Count & Trend
  productCountTemp: any [] = [
    { count: 14, trend: 'up'},
    { count: 48, trend: 'up'},
    { count: 30, trend: 'up'},
    { count: 13, trend: 'up'},
    { count: 5, trend: 'up'},
    { count: 8, trend: 'down'},
    { count: 16, trend: 'down'},
    { count: 26, trend: 'up'},
    { count: 12, trend: 'up'},
    { count: 3, trend: 'down'}
  ];

  constructor( public demoData: DemoDataService ) { }

  ngOnInit() {
  }

}
