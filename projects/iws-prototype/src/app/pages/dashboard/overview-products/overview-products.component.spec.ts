import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewProductsComponent } from './overview-products.component';

describe('OverviewProductsComponent', () => {
  let component: OverviewProductsComponent;
  let fixture: ComponentFixture<OverviewProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
