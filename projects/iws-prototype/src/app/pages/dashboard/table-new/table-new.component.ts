import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ListingService } from '../../../services/listing.service';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-new-orders',
  templateUrl: './table-new.component.html',
  styleUrls: ['./table-new.component.scss']
})
export class TableNewComponent implements OnInit {
  get data(): ListingService {
    return this.listingService;
  }

  set data(data: ListingService) {
    this.listingService = data;
  }

  get demo(): DemoDataService {
    return this.demoData;
  }

  set demo(demo: DemoDataService) {
    this.demoData = demo;
  }

  constructor(
      public listingService: ListingService,
      public demoData: DemoDataService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = this.listingService.dashboardOrderColumns;
  dataSource = new MatTableDataSource(this.demoData.demoOrders);

  ngOnInit() {
    this.dataSource.paginator = this.paginator;

    // Table Sorting
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'contact': {
          const contactSource = JSON.stringify(this.demoData.getContactById(item.contact));
          const contactName = contactSource.split('"');
          return contactName[5];
        }
        default: {
          return item[property];
        }
      }
    };

    this.dataSource.sort = this.sort;

    // Set Active View
    this.listingService.setActiveView('orders');

    // Manage Entries for Edit Table Dropdown
    this.listingService.setTableColumns(this.listingService.orderTableColumns, 'Produkte', false);
    this.listingService.setTableColumns(this.listingService.orderTableColumns, 'Dateien', false);
    this.listingService.setTableColumns(this.listingService.orderTableColumns, 'Status', false);

  }
}
