import {Component, OnInit, ViewChild} from '@angular/core';
import {ListingService} from '../../../services/listing.service';
import {DemoDataService} from '../../../services/demo-data.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'pt-error-files',
  templateUrl: './table-errors.component.html',
  styleUrls: ['./table-errors.component.scss']
})
export class TableErrorsComponent implements OnInit {
  get data(): ListingService {
    return this.listingService;
  }

  set data(data: ListingService) {
    this.listingService = data;
  }

  get demo(): DemoDataService {
    return this.demoData;
  }

  set demo(demo: DemoDataService) {
    this.demoData = demo;
  }

  constructor(
      public listingService: ListingService,
      public demoData: DemoDataService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = this.listingService.dashboardFileColumns;
  dataSource = new MatTableDataSource(this.demoData.demoFiles);

  ngOnInit() {
    this.dataSource.paginator = this.paginator;

    this.dataSource.sort = this.sort;

    // Set Active View
    this.listingService.setActiveView('files');

    // Manage Entries for Edit Table Dropdown
    this.listingService.setTableColumns(this.listingService.fileTableColumns, 'Kontakt', false);
    this.listingService.setTableColumns(this.listingService.fileTableColumns, 'Status', false);
  }
}
