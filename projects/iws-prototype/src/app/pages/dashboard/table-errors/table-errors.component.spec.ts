import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableErrorsComponent } from './table-errors.component';

describe('ErrorFilesComponent', () => {
  let component: TableErrorsComponent;
  let fixture: ComponentFixture<TableErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
