import { Component, HostListener, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { SidebarService } from '../../services/sidebar.service';
import { SettingsService } from '../../services/settings.service';
import { DemoDataService } from '../../services/demo-data.service';
import { ListingService } from '../../services/listing.service';

@Component({
  selector: 'pt-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  get sidebar() {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  constructor(
      public loginService: LoginService,
      public sidebarService: SidebarService,
      public settingsService: SettingsService,
      public demoData: DemoDataService,
      public listingService: ListingService
  ) { }

  public screenWidth$ = new BehaviorSubject<number>
  (window.innerWidth);

  ngOnInit() {

    // Set Active Navigation
    this.loginService.setActivePage('Jobs', false);
    this.loginService.setActivePage('Einstellungen', true);

    // Deactivate Tools
    this.demoData.disableTool('add', true);
    this.demoData.disableTool('export', true);

    // Adjust sidebar visibility depending on screen width.
    this.getScreenWidth().subscribe(width => {
      if (width < this.sidebarService.hideSidebarWidth) {
        this.sidebarService.sidebarMode = 'push';
        this.sidebarService.hasBackdrop = true;
        this.settingsService.isSmallScreen = true;
        this.sidebarService.showSidebar = false;
      } else if (width > this.sidebarService.hideSidebarWidth) {
        this.sidebarService.showSidebar = true;
        this.sidebarService.sidebarMode = 'side';
        this.sidebarService.hasBackdrop = false;
        this.settingsService.isSmallScreen = false;
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }

  getScreenWidth(): Observable<number> {
    return this.screenWidth$.asObservable();
  }

}
