import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { SettingsService } from '../../../../services/settings.service';
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'pt-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  get modal() {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }


  processingSteps: any[] = ['manuell', 'auto'];

  constructor(
      public demoData: DemoDataService,
      public settingsService: SettingsService,
      public modalService: ModalService
  ) { }

  ngOnInit() {
  }

}
