import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { SettingsService } from '../../../../services/settings.service';

@Component({
  selector: 'pt-impose',
  templateUrl: './impose.component.html',
  styleUrls: ['./impose.component.scss']
})
export class ImposeComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public settingsService: SettingsService
  ) { }

  ngOnInit() {
  }

}
