import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../../services/settings.service';

@Component({
  selector: 'pt-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss']
})
export class ModulesComponent implements OnInit {

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  constructor(
      public settingsService: SettingsService
  ) { }

  ngOnInit() {
  }

}
