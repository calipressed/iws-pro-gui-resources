import { Component, OnInit } from '@angular/core';
import {DemoDataService} from '../../../../services/demo-data.service';
import {SettingsService} from '../../../../services/settings.service';
import {ModalService} from '../../../../services/modal.service';

@Component({
  selector: 'pt-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.scss']
})
export class CustomComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  get modal() {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }

  // Order Types
  orderTypes = ['Bestellungen', 'Produkte'];
  selectedType = 'Bestellungen';

  constructor(
      public demoData: DemoDataService,
      public settingsService: SettingsService,
      public modalService: ModalService
  ) { }

  ngOnInit() {
  }

}
