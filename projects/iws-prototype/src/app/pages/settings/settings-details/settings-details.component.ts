import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings.service';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-settings-details',
  templateUrl: './settings-details.component.html',
  styleUrls: ['./settings-details.component.scss']
})
export class SettingsDetailsComponent implements OnInit {

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  constructor(
      public settingsService: SettingsService,
      public demoData: DemoDataService
  ) { }

  ngOnInit() {
  }

}
