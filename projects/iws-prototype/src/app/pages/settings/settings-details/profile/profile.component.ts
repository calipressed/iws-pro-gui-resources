import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../../services/settings.service';
import { DemoDataService } from '../../../../services/demo-data.service';

@Component({
  selector: 'pt-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  get data() {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public settingsService: SettingsService,
  ) { }

  ngOnInit() {
  }

}
