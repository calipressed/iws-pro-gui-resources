import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../../services/demo-data.service';
import { SettingsService } from '../../../../services/settings.service';

@Component({
  selector: 'pt-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }


  constructor(
      public demoData: DemoDataService,
      public settingsService: SettingsService
  ) { }

  ngOnInit() {
  }

}
