import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pt-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss']
})
export class GeneralSettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // Default Values
  defaultMeasure = 'mm';
  defaultReview = 'Review Modul';
  deleteFinished = true;

}
