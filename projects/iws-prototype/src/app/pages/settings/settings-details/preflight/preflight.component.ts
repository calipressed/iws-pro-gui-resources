import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../../services/settings.service';

@Component({
  selector: 'pt-preflight',
  templateUrl: './preflight.component.html',
  styleUrls: ['./preflight.component.scss']
})
export class PreflightComponent implements OnInit {
  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  constructor(
      public settingsService: SettingsService
  ) { }

  public tac = true;

  ngOnInit() {
  }

}
