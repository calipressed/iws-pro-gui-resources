import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../../services/settings.service';
import { DemoDataService } from '../../../../services/demo-data.service';

@Component({
  selector: 'pt-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {
  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  constructor(
      public settingsService: SettingsService,
      public demoData: DemoDataService
  ) { }

  // Default Values for Checkboxes
  activateTrim = true;
  activateBleed = true;
  activatePages = true;

  ngOnInit() {
  }

}
