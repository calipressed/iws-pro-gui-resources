import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../../../services/contacts.service';
import { SettingsService } from '../../../services/settings.service';
import { ModalService } from '../../../services/modal.service';

@Component({
  selector: 'pt-settings-subnavigation',
  templateUrl: './subnavigation.component.html',
  styleUrls: ['./subnavigation.component.scss']
})
export class SubnavigationComponent implements OnInit {

  public deleteMessage;

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  get modal(): ModalService {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }

  constructor(
      public contactData: ContactsService,
      public settingsService: SettingsService,
      public modalService: ModalService
  ) { }

  /**
   * Returns the delete message for the selected entry
   */
  getDeleteMessage() {
    switch (this.settingsService.activeNav.id) {
      case 2:
        this.deleteMessage = 'Möchten Sie diese Einstellung wirklich löschen?';
        break;
      case 3:
        this.deleteMessage = 'Möchten Sie diesen Workflow wirklich löschen?';
        break;
      case 4:
        this.deleteMessage = 'Möchten Sie dieses Preset wirklich löschen?';
        break;
      case 5:
        this.deleteMessage = 'Möchten Sie diesen Wert wirklich löschen?';
        break;
      case 8:
        this.deleteMessage = 'Möchten Sie diesen Benutzer wirklich löschen?';
        break;
      case 10:
        this.deleteMessage = 'Möchten Sie dieses Werkzeug wirklich löschen?';
        break;
    }
  }

  ngOnInit() {
  }

}
