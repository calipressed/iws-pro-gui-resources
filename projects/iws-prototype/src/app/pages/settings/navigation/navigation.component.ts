import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings.service';

@Component({
  selector: 'pt-settings-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  get settings() {
    return this.settingsService;
  }

  set settings(data: SettingsService) {
    this.settingsService = data;
  }

  constructor(
      public settingsService: SettingsService
  ) { }

  ngOnInit() {
  }

}
