import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {DetailsService} from '../../../services/details.service';


@Component({
  selector: 'pt-thumbnails',
  templateUrl: './thumbnails.component.html',
  styleUrls: ['./thumbnails.component.scss']
})
export class ThumbnailsComponent implements OnInit {

  get details(): DetailsService {
    return this.detailsService;
  }

  set details(data: DetailsService) {
    this.detailsService = data;
  }

  constructor(
      public detailsService: DetailsService
  ) { }

  // Thumbnail Container – Thumbnails per Row depends on this elements width.
  @ViewChild('thumbnailWrapper') thumbnailWrapper: ElementRef;

  ngOnInit() {
    this.detailsService.thumbsContainer = this.thumbnailWrapper;
  }
}
