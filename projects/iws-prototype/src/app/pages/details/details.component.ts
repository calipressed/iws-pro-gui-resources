import { Component, HostListener, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { DemoDataService } from '../../services/demo-data.service';
import { ListingService } from '../../services/listing.service';
import { DetailsService } from '../../services/details.service';

@Component({
  selector: 'pt-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  // Manage Visibility for Vertical Tabs
  sidebarNavigation: any[] = [
    {name: 'tools', show: true},
    {name: 'preflight', show: false},
    {name: 'order', show: false},
    {name: 'products', show: false},
  ];

  public screenWidth$ = new BehaviorSubject<number>
  (window.innerWidth);

  get sidebar(): SidebarService {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get details(): DetailsService {
    return this.detailsService;
  }

  set details(data: DetailsService) {
    this.detailsService = data;
  }

  constructor(
      public sidebarService: SidebarService,
      public demoData: DemoDataService,
      public listingService: ListingService,
      public detailsService: DetailsService
  ) { }

  /**
   * Show the selected sidebar content.
   *
   * @remarks Hides the sidebar if the selected sidebar is already active.
   *
   * @param elemName – The sidebar content to be shown.
   */
  showSidebarContent(elemName) {
    this.sidebarNavigation.forEach((elem, index) => {
      if (elem.name === elemName) {
        if (elem.show === true) {
          this.sidebarService.showSidebar = !this.sidebarService.showSidebar;
        } else {
          this.sidebarService.showSidebar = true;
        }
        this.sidebarNavigation[index].show = true;
      } else {
        this.sidebarNavigation[index].show = false;
      }
    });
  }

  ngOnInit() {

    // Active Page
    this.listingService.activePage = 'details';

    // Active File
    this.listingService.currentFile =  this.demoData.demoFiles[0];
    this.listingService.currentProduct = this.demoData.getProductById(this.listingService.currentFile['productId']);
    this.listingService.currentProducts = this.demoData.getProductsByOrderId(this.listingService.currentProduct['orderId']);
    this.listingService.activeFiles = this.demoData.getFilesByProductId(this.listingService.currentProduct['id']);

    // Hide Tools
    this.demoData.hideTool('home', true);
    this.demoData.hideTool('add', false);
    this.demoData.hideTool('delete', false);
    this.demoData.hideTool('edit', false);
    this.demoData.hideTool('copy', false);
    this.demoData.hideTool('group', false);
    this.demoData.hideTool('sidebar', false);
    this.demoData.hideTool('custom', true);
    this.demoData.hideTool('export', false);

    // Activate Tools
    this.demoData.disableTool('mail', true);

    // Add Demo Entry to Enable Impose & Print
    this.listingService.setActiveRow('demo');


    // Adjust sidebar visibility depending on screen width.
    this.getScreenWidth().subscribe(width => {
      if (width < this.sidebarService.hideSidebarWidth) {
        this.sidebarService.showSidebar = false;
        this.sidebarService.sidebarMode = 'push';
        this.sidebarService.hasBackdrop = true;
      } else if (width > this.sidebarService.hideSidebarWidth) {
        this.sidebarService.showSidebar = true;
        this.sidebarService.sidebarMode = 'side';
        this.sidebarService.hasBackdrop = false;
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.detailsService.calcThumbsPerRow();
    this.screenWidth$.next(event.target.innerWidth);
  }

  getScreenWidth(): Observable<number> {
    return this.screenWidth$.asObservable();
  }
}
