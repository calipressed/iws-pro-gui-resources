import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../services/listing.service';
import { DetailsService } from '../../../services/details.service';

@Component({
  selector: 'pt-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})

export class PreviewComponent implements OnInit {

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get details(): DetailsService {
    return this.detailsService;
  }

  set details(data: DetailsService) {
    this.detailsService = data;
  }

  // Default Zoom Value
  defaultZoom = '100%';

  constructor(
      public listingService: ListingService,
      public detailsService: DetailsService
  ) {}

  ngOnInit() {
  }

}
