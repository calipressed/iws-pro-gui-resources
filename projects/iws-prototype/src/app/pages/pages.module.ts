import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { ListingComponent } from './listing/listing.component';
import { FilesComponent } from './listing/files/files.component';
import { ProductsComponent } from './listing/products/products.component';
import { OrdersComponent } from './listing/orders/orders.component';
import { StatusSelectionComponent } from './listing/status-selection/status-selection.component';
import { EditTableComponent } from './listing/edit-table/edit-table.component';
import { SelectedEntriesComponent } from './listing/selected-entries/selected-entries.component';
import { LoginComponent } from './login/login.component';
import { LoginMaskComponent } from './login/login-mask/login-mask.component';
import { PwdSendComponent } from './login/pwd-send/pwd-send.component';
import { PwdForgotComponent } from './login/pwd-forgot/pwd-forgot.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ContactsComponent } from './contacts/contacts.component';
import { SettingsComponent } from './settings/settings.component';
import { TableNewComponent } from './dashboard/table-new/table-new.component';
import { TableErrorsComponent } from './dashboard/table-errors/table-errors.component';
import { OverviewFilesComponent } from './dashboard/overview-files/overview-files.component';
import { OverviewProductsComponent } from './dashboard/overview-products/overview-products.component';
import { OverviewOrdersComponent } from './dashboard/overview-orders/overview-orders.component';
import { DetailsComponent } from './details/details.component';
import { PreviewComponent } from './details/preview/preview.component';
import { ThumbnailsComponent } from './details/thumbnails/thumbnails.component';
import { ContactDetailsComponent } from './contacts/contact-details/contact-details.component';
import { ListContactsComponent } from './contacts/list-contacts/list-contacts.component';
import { ListCompaniesComponent } from './contacts/list-companies/list-companies.component';
import { CompanyDetailsComponent } from './contacts/company-details/company-details.component';
import { GroupModalComponent } from './contacts/group-new/group-new.component';
import { GroupEditComponent } from './contacts/group-edit/group-edit.component';
import { NavigationComponent } from './settings/navigation/navigation.component';
import { SubnavigationComponent } from './settings/subnavigation/subnavigation.component';
import { SettingsDetailsComponent } from './settings/settings-details/settings-details.component';
import { ProfileComponent } from './settings/settings-details/profile/profile.component';
import { PreflightComponent } from './settings/settings-details/preflight/preflight.component';
import { DeveloperComponent } from './settings/settings-details/developer/developer.component';
import { ToolsComponent } from './settings/settings-details/tools/tools.component';
import { UserComponent } from './settings/settings-details/user/user.component';
import { ModulesComponent } from './settings/settings-details/modules/modules.component';
import { GeneralSettingsComponent } from './settings/settings-details/general-settings/general-settings.component';
import { ImposeComponent } from './settings/settings-details/impose/impose.component';
import { WorkflowComponent } from './settings/settings-details/workflow/workflow.component';
import { CustomComponent } from './settings/settings-details/custom/custom.component';

@NgModule({
  declarations: [
      ListingComponent,
      FilesComponent,
      ProductsComponent,
      OrdersComponent,
      StatusSelectionComponent,
      EditTableComponent,
      SelectedEntriesComponent,
      LoginComponent,
      LoginMaskComponent,
      PwdSendComponent,
      PwdForgotComponent,
      DashboardComponent,
      ContactsComponent,
      SettingsComponent,
      TableNewComponent,
      TableErrorsComponent,
      OverviewFilesComponent,
      OverviewProductsComponent,
      OverviewOrdersComponent,
      DetailsComponent,
      PreviewComponent,
      ThumbnailsComponent,
      ContactDetailsComponent,
      ListContactsComponent,
      ListCompaniesComponent,
      CompanyDetailsComponent,
      GroupModalComponent,
      GroupEditComponent,
      NavigationComponent,
      SubnavigationComponent,
      SettingsDetailsComponent,
      ProfileComponent,
      PreflightComponent,
      DeveloperComponent,
      ToolsComponent,
      UserComponent,
      ModulesComponent,
      GeneralSettingsComponent,
      ImposeComponent,
      WorkflowComponent,
      CustomComponent
  ],
  imports: [
    CommonModule,
    LayoutModule
  ],
  exports: [
    ListingComponent,
    LayoutModule
  ]
})
export class PagesModule { }
