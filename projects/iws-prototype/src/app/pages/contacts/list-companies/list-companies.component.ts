import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ContactsService } from '../../../services/contacts.service';
import {ModalService} from '../../../services/modal.service';

@Component({
  selector: 'pt-list-companies',
  templateUrl: './list-companies.component.html',
  styleUrls: ['./list-companies.component.scss']
})
export class ListCompaniesComponent implements OnInit {

  public deleteMessage;

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  get modal(): ModalService {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public contactData: ContactsService,
      public modalService: ModalService
  ) { }

  /**
   * Returns the delete message for groups & companies.
   */
  getDeleteMessage(type) {
    if (type === 'group') {
      this.deleteMessage = 'Möchten Sie die ausgewählte Kundengruppe wirklich löschen?';
    } else {
      this.deleteMessage = 'Möchten Sie die ausgewählte Firma wirklich löschen?';
    }
  }

  ngOnInit() {
  }

}
