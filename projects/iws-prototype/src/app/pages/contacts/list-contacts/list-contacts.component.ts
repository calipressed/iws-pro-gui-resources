import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../../../services/contacts.service';
import { DemoDataService } from '../../../services/demo-data.service';
import {ModalService} from '../../../services/modal.service';

@Component({
  selector: 'pt-list-contacts',
  templateUrl: './list-contacts.component.html',
  styleUrls: ['./list-contacts.component.scss']
})
export class ListContactsComponent implements OnInit {

  public deleteMessage;

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  get modal(): ModalService {
    return this.modalService;
  }

  set modal(data: ModalService) {
    this.modalService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public contactData: ContactsService,
      public modalService: ModalService
  ) { }

  /**
   * Returns the delete message for groups & companies.
   */
  getDeleteMessage(type) {
    this.deleteMessage = 'Möchten Sie diesen Kontakt wirklich löschen?';
  }

  ngOnInit() {
  }

}
