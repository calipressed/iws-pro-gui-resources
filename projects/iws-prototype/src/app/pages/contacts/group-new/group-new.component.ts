import {Component, OnInit} from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ContactsService } from '../../../services/contacts.service';

@Component({
  selector: 'pt-group-new',
  templateUrl: './group-new.component.html',
  styleUrls: ['./group-new.component.scss']
})
export class GroupModalComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  constructor(
      public demoData: DemoDataService,
      public contactData: ContactsService
  ) { }

  ngOnInit() {
  }

}
