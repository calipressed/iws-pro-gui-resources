import {Component, HostListener, OnInit} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { DemoDataService } from '../../services/demo-data.service';
import { SidebarService } from '../../services/sidebar.service';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'pt-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get sidebar() {
    return this.sidebarService;
  }

  set sidebar(data: SidebarService) {
    this.sidebarService = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }


  get login(): LoginService {
    return this.loginService;
  }

  set login(data: LoginService) {
    this.loginService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public sidebarService: SidebarService,
      public contactData: ContactsService,
      public loginService: LoginService
  ) { }

  public screenWidth$ = new BehaviorSubject<number>
  (window.innerWidth);

  ngOnInit() {
    // Set Active Navigation
    this.loginService.setActivePage('Jobs', false);
    this.loginService.setActivePage('Kontakte', true);

    // Deactivate Tools
    this.demoData.disableTool('add', true);
    this.demoData.disableTool('export', true);

    // Adjust sidebar visibility depending on screen width.
    this.getScreenWidth().subscribe(width => {
      if (width < this.sidebarService.hideSidebarWidth) {
        this.sidebarService.sidebarMode = 'push';
        this.sidebarService.hasBackdrop = true;
        this.contactData.isSmallScreen = true;
      } else if (width > this.sidebarService.hideSidebarWidth) {
        this.sidebarService.showSidebar = true;
        this.sidebarService.sidebarMode = 'side';
        this.sidebarService.hasBackdrop = false;
        this.contactData.isSmallScreen = false;
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }

  getScreenWidth(): Observable<number> {
    return this.screenWidth$.asObservable();
  }
}
