import { Component, OnInit } from '@angular/core';
import { ContactsService} from '../../../services/contacts.service';
import { DemoDataService } from '../../../services/demo-data.service';

@Component({
  selector: 'pt-new-contact',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {

  get demo(): DemoDataService {
    return this.demoData;
  }

  set demo(data: DemoDataService) {
    this.demoData = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  constructor(
      public contactData: ContactsService,
      public demoData: DemoDataService
  ) { }

  ngOnInit() {
  }
}
