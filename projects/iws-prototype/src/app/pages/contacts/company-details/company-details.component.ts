import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ContactsService } from '../../../services/contacts.service';

@Component({
  selector: 'pt-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {

  get demo(): DemoDataService {
    return this.demoData;
  }

  set demo(data: DemoDataService) {
    this.demoData = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  constructor(
      public contactData: ContactsService,
      public demoData: DemoDataService
  ) { }

  ngOnInit() {
  }

}
