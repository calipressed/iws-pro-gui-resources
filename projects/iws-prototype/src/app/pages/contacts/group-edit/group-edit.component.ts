import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ContactsService } from '../../../services/contacts.service';

@Component({
  selector: 'pt-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.scss']
})
export class GroupEditComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get contact(): ContactsService {
    return this.contactData;
  }

  set contact(data: ContactsService) {
    this.contactData = data;
  }

  constructor(
      public demoData: DemoDataService,
      public contactData: ContactsService
  ) { }

  ngOnInit() {
  }

}
