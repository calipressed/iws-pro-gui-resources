import { Component, OnInit, HostListener } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { ListingService } from '../../services/listing.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { DemoDataService } from '../../services/demo-data.service';


@Component({
  selector: 'pt-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  get data() {
      return this.sidebarService;
  }

  get demo() {
      return this.demoData;
  }

  get listing() {
      return this.listingService;
  }

  set listing(listing: ListingService) {
      this.listingService = listing;
  }

  constructor(
      public sidebarService: SidebarService,
      public demoData: DemoDataService,
      public listingService: ListingService
  ) { }

  // Sidebar visibility
  showSidebar = this.sidebarService.showSidebar;

  public screenWidth$ = new BehaviorSubject<number>
  (window.innerWidth);


  ngOnInit() {

      // Adjust sidebar visibility depending on screen width.
      this.getScreenWidth().subscribe(width => {
          if (width < this.sidebarService.hideSidebarWidth) {
              this.sidebarService.showSidebar = false;
              this.sidebarService.sidebarMode = 'push';
              this.sidebarService.hasBackdrop = true;
          } else if (width > this.sidebarService.hideSidebarWidth) {
              this.sidebarService.showSidebar = true;
              this.sidebarService.sidebarMode = 'side';
              this.sidebarService.hasBackdrop = false;
          }
      });
  }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenWidth$.next(event.target.innerWidth);
    }

    getScreenWidth(): Observable<number> {
        return this.screenWidth$.asObservable();
    }
}
