import { Component, OnInit, Input } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ListingService } from '../../../services/listing.service';

@Component({
  selector: 'pt-edit-table',
  templateUrl: './edit-table.component.html',
  styleUrls: ['./edit-table.component.scss']
})

export class EditTableComponent implements OnInit {

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  @Input() tableData: string;


  constructor(
      public listingService: ListingService,
      public demoData: DemoDataService
  ) { }

  ngOnInit() {
  }

}
