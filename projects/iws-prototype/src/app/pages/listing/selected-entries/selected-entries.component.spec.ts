import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedEntriesComponent } from './selected-entries.component';

describe('SelectedEntriesComponent', () => {
  let component: SelectedEntriesComponent;
  let fixture: ComponentFixture<SelectedEntriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedEntriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedEntriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
