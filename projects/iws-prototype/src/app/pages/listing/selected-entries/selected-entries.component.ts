import { Component, OnInit } from '@angular/core';
import { DemoDataService } from '../../../services/demo-data.service';
import { ListingService } from '../../../services/listing.service';

@Component({
  selector: 'pt-selected-entries',
  templateUrl: './selected-entries.component.html',
  styleUrls: ['./selected-entries.component.scss']
})
export class SelectedEntriesComponent implements OnInit {

  get data(): DemoDataService {
    return this.demoData;
  }

  set data(data: DemoDataService) {
    this.demoData = data;
  }

  get listing(): ListingService {
    return this.listingService;
  }

  set listing(data: ListingService) {
    this.listingService = data;
  }

  constructor(
      public demoData: DemoDataService,
      public listingService: ListingService
  ) {}

  ngOnInit() {
  }

}
