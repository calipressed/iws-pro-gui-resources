import { Component, OnInit, ViewChild } from '@angular/core';
import { ListingService } from '../../../services/listing.service';
import { SidebarService } from '../../../services/sidebar.service';
import { DemoDataService } from '../../../services/demo-data.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'pt-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})

export class OrdersComponent implements OnInit {
    get data(): ListingService {
        return this.listingService;
    }

    set data(data: ListingService) {
        this.listingService = data;
    }

    get demo(): DemoDataService {
        return this.demoData;
    }

    set demo(demo: DemoDataService) {
        this.demoData = demo;
    }

    get side(): SidebarService {
        return this.sidebar;
    }

    set side(side: SidebarService) {
        this.sidebar = side;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService,
        public sidebar: SidebarService,
    ) { }

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    displayedColumns = this.listingService.displayedOrderColumns;
    dataSource = new MatTableDataSource(this.demoData.demoOrders);

  ngOnInit() {
      this.dataSource.paginator = this.paginator;

      // Table Sorting
      this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
              case 'contact': {
                  const contactSource = JSON.stringify(this.demoData.getContactById(item.contact));
                  const contactName = contactSource.split('"');
                  return contactName[5];
              }
              case 'productCount': {
                  return this.demoData.countProducts(item.id);
              }
              case 'fileCount': {
                  return this.demoData.countAll(item.id);
              }
              default: {
                  return item[property];
              }
          }
      };

      this.dataSource.sort = this.sort;

      // Manage Entries for Edit Table Dropdown
      this.listingService.setTableColumns(this.listingService.orderTableColumns, 'Produkte', true);
      this.listingService.setTableColumns(this.listingService.orderTableColumns, 'Dateien', true);
      this.listingService.setTableColumns(this.listingService.orderTableColumns, 'Status', true);
  }

}
