import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../services/listing.service';

@Component({
  selector: 'pt-status-selection',
  templateUrl: './status-selection.component.html',
  styleUrls: ['./status-selection.component.scss']
})

export class StatusSelectionComponent implements OnInit {
    get data() {
        return this.listingService;
    }

    constructor( public listingService: ListingService) { }

    statusRange = this.listingService.statusRange;

  ngOnInit() {
  }

}
