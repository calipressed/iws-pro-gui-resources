import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ListingService } from '../../../services/listing.service';
import { SidebarService } from '../../../services/sidebar.service';
import { DemoDataService } from '../../../services/demo-data.service';


@Component({
  selector: 'pt-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})

export class FilesComponent implements OnInit {

    get data(): ListingService {
        return this.listingService;
    }

    set data(data: ListingService) {
        this.listingService = data;
    }

    get demo(): DemoDataService {
        return this.demoData;
    }

    set demo(demo: DemoDataService) {
        this.demoData = demo;
    }

    constructor(
        public listingService: ListingService,
        public sidebar: SidebarService,
        public demoData: DemoDataService
    ) { }

    // Status Range
    statusRange = this.listingService.statusRange;

    // Table
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    displayedColumns = this.listingService.displayedFileColumns;


    ngOnInit() {
        this.listingService.setFileType();
        this.listingService.fileSource.paginator = this.paginator;

        // Table Sorting
        this.listingService.fileSource.sortingDataAccessor = (item, property) => {

            switch (property) {
                case 'contact': {
                    const contactSource = JSON.stringify(this.demoData.buildContact(item.productId));
                    const contactName = contactSource.split('"');
                    return contactName[5];
                }
                default: {
                    return item[property];
                }
            }
        };

        this.listingService.fileSource.sort = this.sort;
    }
}