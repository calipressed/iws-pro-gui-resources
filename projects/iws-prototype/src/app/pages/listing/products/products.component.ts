import { Component, OnInit, ViewChild } from '@angular/core';
import { ListingService } from '../../../services/listing.service';
import { DemoDataService } from '../../../services/demo-data.service';
import { SidebarService } from '../../../services/sidebar.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'pt-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})

export class ProductsComponent implements OnInit {

    get data(): ListingService {
        return this.listingService;
    }

    set data(data: ListingService) {
        this.listingService = data;
    }

    get demo(): DemoDataService {
        return this.demoData;
    }

    set demo(demo: DemoDataService) {
        this.demoData = demo;
    }

    get side(): SidebarService {
        return this.sidebar;
    }

    set side(side: SidebarService) {
        this.sidebar = side;
    }

    constructor(
        public listingService: ListingService,
        public demoData: DemoDataService,
        public sidebar: SidebarService,
    ) { }

    // Table
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    displayedColumns = this.listingService.displayedProductColumns;
    dataSource = new MatTableDataSource(this.demoData.demoProducts);

  ngOnInit() {
      this.dataSource.paginator = this.paginator;

      // Table Sorting
      this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
              case 'contact': {
                  const contactSource = JSON.stringify(this.demoData.buildContact(item.id));
                  const contactName = contactSource.split('"');
                  return contactName[5];
              }
              case 'fileCount': {
                  return this.demoData.countFiles(item.id);
              }
              default: {
                  return item[property];
              }
          }
      };

      this.dataSource.sort = this.sort;
  }

}
