# We label our stage as ‘builder’
FROM node:latest as builder

LABEL maintainer="Heinrich Reimer <heinrich@calibrate.at>"

# Packages
#RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list

RUN apt-get update \
 && apt-get install -y bash git openssh-server \
 && update-ca-certificates

# Build time
ARG PORT=8080
ARG SSH_PRV_KEY
ARG SSH_PUB_KEY

# Authorize SSH Host
RUN mkdir -p /root/.ssh && \
chmod 0700 /root/.ssh && \
ssh-keyscan bitbucket.org > /root/.ssh/known_hosts

# Add the keys and set permissions
RUN echo "$SSH_PRV_KEY" > /root/.ssh/id_rsa && \
echo "$SSH_PUB_KEY" > /root/.ssh/id_rsa.pub && \
chmod 600 /root/.ssh/id_rsa && \
chmod 600 /root/.ssh/id_rsa.pub

RUN git config --global url.ssh://git@bitbucket.org/.insteadOf https://bitbucket.org/

# Stage 1: Build the app in image 'builder'

# Workdir
#RUN mkdir -p /root/node/iws-pro-gui
#ADD . /root/node/iws-pro-gui 
WORKDIR /root/node/iws-pro-gui

COPY package*.json ./
COPY . .
WORKDIR /root/node/iws-pro-gui/src
RUN npm ci
RUN npm rebuild node-sass
RUN npm run build

# Stage 2: Use build output from 'builder'

FROM nginx:stable-alpine
LABEL version="1.0"

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/nginx.conf

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

WORKDIR /usr/share/nginx/html
## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /root/node/iws-pro-gui/dist/iws-ui .

EXPOSE $PORT

CMD ["nginx", "-g", "daemon off;"]

