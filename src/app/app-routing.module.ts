import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LibraryShared } from '../../projects/iws-library/src/app/app.module';
import { PrototypeShared } from '../../projects/iws-prototype/src/app/app.module';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
    {
      path: '',
      redirectTo: 'welcome',
      pathMatch: 'full'
    },
    {
        path: 'welcome',
        component: WelcomeComponent
    },
    {
        path: 'prototype',
        loadChildren: '../../projects/iws-prototype/src/app/app.module#PrototypeShared'
    },
    {
        path: 'library',
        loadChildren: '../../projects/iws-library/src/app/app.module#LibraryShared'
    }
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes),
      PrototypeShared.forRoot(),
      LibraryShared.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
