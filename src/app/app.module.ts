import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrototypeShared } from '../../projects/iws-prototype/src/app/app.module';
import { LibraryShared } from '../../projects/iws-library/src/app/app.module';
import { WelcomeComponent } from './welcome/welcome.component';


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PrototypeShared.forRoot(),
    LibraryShared.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
